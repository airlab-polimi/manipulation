#include <iostream>
#include <sstream>
#include <opencv2/aruco.hpp>
#include <opencv2/opencv.hpp>

void create_marker()
{
	cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);

	std::vector<cv::Mat> marker;
    marker.resize(13);

	cv::aruco::drawMarker(dictionary,  0, 150, marker.at(0), 1);
	cv::aruco::drawMarker(dictionary,  1, 150, marker.at(1), 1);
	cv::aruco::drawMarker(dictionary,  2, 150, marker.at(2), 1);
	cv::aruco::drawMarker(dictionary,  3, 150, marker.at(3), 1);
	cv::aruco::drawMarker(dictionary,  4, 150, marker.at(4), 1);
	cv::aruco::drawMarker(dictionary,  5, 150, marker.at(5), 1);
	cv::aruco::drawMarker(dictionary,  6, 150, marker.at(6), 1);
	cv::aruco::drawMarker(dictionary,  7, 150, marker.at(7), 1);
	cv::aruco::drawMarker(dictionary,  8, 150, marker.at(8), 1);
	cv::aruco::drawMarker(dictionary,  9, 150, marker.at(9), 1);
	cv::aruco::drawMarker(dictionary, 10, 150, marker.at(10), 1);
	cv::aruco::drawMarker(dictionary, 11, 150, marker.at(11), 1);
	cv::aruco::drawMarker(dictionary, 12, 150, marker.at(12), 1);

	std::ostringstream convert;

    std::string imageName = "./images/6x6Marker_";
    for (int i=0; i<marker.size(); i++) {
        convert.str("");
        convert << imageName  << i << ".jpg";
        imwrite(convert.str(), marker.at(i));
    }
}

int main(int argc, char **argv)
{
    create_marker();

    return 0;
}
