close all
clear all

%% STEP 1
% Posizionare il marker in una posizione "comoda" dove sia possibile
% misurare la posizione lungo x-y-z (terna robot) del corner 0 del marker
% rispetto all'origine della terna. In alternativa, posizionare il marker e
% guidare manualmente il robot in modo che l'end effector raggiunga il
% marker, quindi leggere da topic (j2n6s300_driver/out/tool_pose) la posizione dell'end effector (considare offset di dita e unghie)

P = [ 0.0435 ; -0.042 ; -0.036 ]; % Posizione corner 0 del marker rispetto alla terna base /j2n6s300_link_base

%% STEP 2
% Posizionare il braccio cosi che la camera inquadri il marker: leggere e salvare dal
% topic "/dispenser_imgproc/aruco_cornersCamera" la posizione del corner
% 0 del marker, e da "j2n6s300_driver/out/tool_pose" posizione e orientamento(quaternione) dell'end effector. Successivamente, modificare la posizione dell'end effector
% mantenendo costante l'orientamento (rosrun kinova_demo pose_action_client.py -v -r j2n6s300 mdeg -- "delta_x" "delta_y" "delta_z" 0 0 0)
% quindi salvare nuovamente la posizione del corner e posizione dell'end
% effector. Ripetere questa sequenza fino ad aver registrato 6 punti

Pp_c1 = [-0.242328763008; -0.134865313768; 0.53100001812]; % coordinate corner 0 del marker in camera frame (x,y,z)
Pp_c2 = [-0.16216082871; -0.159184902906; 0.543000042439];
Pp_c3 = [-0.0674698650837; -0.178555518389; 0.54800003767];
Pp_c4 = [-0.0581138134003; -0.127625763416; 0.448000013828];
Pp_c5 = [0.0414672791958; -0.141354829073; 0.451000034809];
Pp_c6 = [-0.156019598246; -0.0987582355738; 0.442000031471];
Pp_c8 = [0.0217758119106; -0.184628129005; 0.583000004292];
Pp_c9 = [0.0364533513784; -0.100316628814; 0.576000034809];
Pp_c10 = [0.0524739250541; -0.0799827799201; 0.401000022888];

Peef_o1 = [0.258595257998; 0.121421851218; 0.5063174963]; % posizione end-effector rispetto alla base
Peef_o2 = [0.260967373848; 0.0233513563871; 0.506380915642];
Peef_o3 = [0.258790582418; -0.0764292925596; 0.506542801857];
Peef_o4 = [0.212712064385; -0.076287150383; 0.407950758934];
Peef_o5 = [0.21001803875; -0.173676133156; 0.407612651587];
Peef_o6 = [0.209972947836; 0.0281129088253; 0.407769918442];
Peef_o7 = [0.220621943474; 0.0322314426303; 0.548500537872];
Peef_o8 = [0.221442431211; -0.168581575155; 0.541113257408];
Peef_o9 = [0.129522532225; -0.174654722214; 0.526069164276];
Peef_o10 = [0.129445552826; -0.174069806933; 0.357158869505];

quat = [0.020, -0.717; 0.696; 0.007]; %orientamento end-effector rispetto alla base (quaternion wxyz)

quatnormalize(quat);
Reef_o = quat2rotm(quat);

%% PLOT
% figure; hold on; 
% grid; xlabel('x'); ylabel('y'); zlabel('z'); scatter3(P(1),P(2),P(3),'red');
% scatter3(Peef_o1(1),Peef_o1(2),Peef_o1(3),'blue');scatter3(Peef_o2(1),Peef_o2(2),Peef_o2(3),'blue');
% scatter3(Peef_o3(1),Peef_o3(2),Peef_o3(3),'green');scatter3(Peef_o4(1),Peef_o4(2),Peef_o4(3),'green');
% scatter3(Peef_o5(1),Peef_o5(2),Peef_o5(3),'yellow');scatter3(Peef_o6(1),Peef_o6(2),Peef_o6(3),'yellow');

%% STEP 3
% Procedura per calcolare la matrice di rotazione tra camera ed end
% effector
alpha1 = Reef_o'*(Peef_o2-Peef_o1);
alpha2 = Reef_o'*(Peef_o4-Peef_o3);
alpha3 = Reef_o'*(Peef_o6-Peef_o5);
alpha4 = Reef_o'*(Peef_o8-Peef_o7);
alpha5 = Reef_o'*(Peef_o10-Peef_o9);
alpha_bar = [alpha1;alpha2;alpha3;alpha4;alpha5]; %vettore colonna (9x1)
beta1 = Pp_c1-Pp_c2;
beta2 = Pp_c3-Pp_c4;
beta3 = Pp_c5-Pp_c6;
beta4 = Pp_c7-Pp_c8;
beta5 = Pp_c9-Pp_c10;
beta_bar = [beta1(1) beta1(2) beta1(3) 0 0 0 0 0 0          %matrice (9x9)
            0 0 0 beta1(1) beta1(2) beta1(3) 0 0 0
            0 0 0 0 0 0 beta1(1) beta1(2) beta1(3)
            beta2(1) beta2(2) beta2(3) 0 0 0 0 0 0
            0 0 0 beta2(1) beta2(2) beta2(3) 0 0 0
            0 0 0 0 0 0 beta2(1) beta2(2) beta2(3)
            beta3(1) beta3(2) beta3(3) 0 0 0 0 0 0
            0 0 0 beta3(1) beta3(2) beta3(3) 0 0 0
            0 0 0 0 0 0 beta3(1) beta3(2) beta3(3)
            beta4(1) beta4(2) beta4(3) 0 0 0 0 0 0
            0 0 0 beta4(1) beta4(2) beta4(3) 0 0 0
            0 0 0 0 0 0 beta4(1) beta4(2) beta4(3)
            beta5(1) beta5(2) beta5(3) 0 0 0 0 0 0
            0 0 0 beta5(1) beta5(2) beta5(3) 0 0 0
            0 0 0 0 0 0 beta5(1) beta5(2) beta5(3)];        

% sistema beta_bar*r=alpha_bar (Ax=b)-> r = beta_bar^-1*alpha_bar (x=A\B)
r = beta_bar\alpha_bar;
Rcamera = [r(1) r(2) r(3);           %%ricostruzione matrice Rc_eef
           r(4) r(5) r(6);
           r(7) r(8) r(9)];

%% STEP 4
% Procedura di normalizzazione della matrice
X = transpose(Rcamera(1,:));
Y = transpose(Rcamera(2,:));
error = dot(X,Y);
Xorth = X - (error/2)*Y;
Yorth = Y - (error/2)*X;
Zorth = cross(Xorth,Yorth);

Xnorm = 1/2*(3-dot(Xorth,Xorth))*Xorth;
Ynorm = 1/2*(3-dot(Yorth,Yorth))*Yorth;
Znorm = 1/2*(3-dot(Zorth,Zorth))*Zorth;

Xnorm = transpose(Xnorm);
Ynorm = transpose(Ynorm);
Znorm = transpose(Znorm);

R = [Xnorm
    Ynorm
    Znorm];

%% NORMALIZATION (procedura 2)
% a = Rcamera(:,1);
% b = Rcamera(:,2);
% A = a/norm(a);
% B = b - dot(a,b)*a;
% B = B/norm(B);
% C = cross(a,b);
% Rnorm = [A B C]
 
%% NORMALIZATION (procedura 3)
% A = a/norm(a);
% c = Rcamera(:,3);
% B = cross(c,a);
% B = B/norm(B);
% C = cross(A,B);
% Rnorm2 = [A B C]

%% Results
%R = eul2rotm([170*pi/180, 0, 0]);

Pc_eef = mean([Reef_o'*(P-Peef_o1-Reef_o*R*Pp_c1), Reef_o'*(P-Peef_o2-Reef_o*R*Pp_c2), Reef_o'*(P-Peef_o3-Reef_o*R*Pp_c3), Reef_o'*(P-Peef_o4-Reef_o*R*Pp_c4), ...
               Reef_o'*(P-Peef_o5-Reef_o*R*Pp_c5), Reef_o'*(P-Peef_o6-Reef_o*R*Pp_c6), Reef_o'*(P-Peef_o7-Reef_o*R*Pp_c7), Reef_o'*(P-Peef_o8-Reef_o*R*Pp_c8), ...
               Reef_o'*(P-Peef_o9-Reef_o*R*Pp_c9), Reef_o'*(P-Peef_o10-Reef_o*R*Pp_c10)],2)
Rc_eef = R

%% Verify calibration
Pe_1 = Peef_o1+Reef_o*Pc_eef+Reef_o*Rc_eef*Pp_c1;
Pe_2 = Peef_o2+Reef_o*Pc_eef+Reef_o*Rc_eef*Pp_c2;
Pe_3 = Peef_o3+Reef_o*Pc_eef+Reef_o*Rc_eef*Pp_c3;
Pe_4 = Peef_o4+Reef_o*Pc_eef+Reef_o*Rc_eef*Pp_c4;
Pe_5 = Peef_o5+Reef_o*Pc_eef+Reef_o*Rc_eef*Pp_c5;
Pe_6 = Peef_o6+Reef_o*Pc_eef+Reef_o*Rc_eef*Pp_c6;
Pe_7 = Peef_o7+Reef_o*Pc_eef+Reef_o*Rc_eef*Pp_c7;
Pe_8 = Peef_o8+Reef_o*Pc_eef+Reef_o*Rc_eef*Pp_c8;
Pe_9 = Peef_o9+Reef_o*Pc_eef+Reef_o*Rc_eef*Pp_c9;
Pe_10 = Peef_o10+Reef_o*Pc_eef+Reef_o*Rc_eef*Pp_c10;
err = [P-Pe_1, P-Pe_2, P-Pe_3, P-Pe_4, P-Pe_5, P-Pe_6, P-Pe_7, P-Pe_8, P-Pe_9, P-Pe_10];
disp(['mean error x: ', mat2str(mean(abs(err(1,:))))]);
disp(['mean error y: ', mat2str(mean(abs(err(2,:))))]);
disp(['mean error z: ', mat2str(mean(abs(err(3,:))))]);
%disp(['x = ',mat2str(P-Pe_1
%disp(['P: ', mat2str(P), ' - Pest: ', mat2str(Pe_1), ' - err: ', mat2str(P-Pe_1)]);
%disp(['P: ', mat2str(P), ' - Pest: ', mat2str(Pe_2), ' - err: ', mat2str(P-Pe_2)]);
%disp(['P: ', mat2str(P), ' - Pest: ', mat2str(Pe_3), ' - err: ', mat2str(P-Pe_3)]);
%disp(['P: ', mat2str(P), ' - Pest: ', mat2str(Pe_4), ' - err: ', mat2str(P-Pe_4)]);
%disp(['P: ', mat2str(P), ' - Pest: ', mat2str(Pe_5), ' - err: ', mat2str(P-Pe_5)]);
%disp(['P: ', mat2str(P), ' - Pest: ', mat2str(Pe_6), ' - err: ', mat2str(P-Pe_6)]);
%disp(['err: ', mat2str(P-Pe_1)]);
%disp(['err: ', mat2str(P-Pe_2)]);
%disp(['err: ', mat2str(P-Pe_3)]);
%disp(['err: ', mat2str(P-Pe_4)]);
%disp(['err: ', mat2str(P-Pe_5)]);
%disp(['err: ', mat2str(P-Pe_6)]);
