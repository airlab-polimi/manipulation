function jointpos_DH = jointpos_jaco2dh(jointpos_jaco)

q1 = -jointpos_jaco(1)*pi/180;
q2 = jointpos_jaco(2)*pi/180 - pi/2;
q3 = jointpos_jaco(3)*pi/180 + pi/2;
q4 = jointpos_jaco(4)*pi/180;
q5 = jointpos_jaco(5)*pi/180 - pi;
q6 = jointpos_jaco(6)*pi/180 + pi/2;

jointpos_DH = [q1, q2, q3, q4, q5, q6]';