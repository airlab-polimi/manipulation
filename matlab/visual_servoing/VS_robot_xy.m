clear all
close all
%% Setup
startup_rvc
fx = 309.341;
fy = 312.294;
ppx = 155.386;
ppy = 120.498;
jaco2 = jaco2_definition();
Pc_eef = [0;0.03;-0.05];
rot = [0,0,0];
Rc_eef = eul2rotm(rot);

%% Features (world)
A = [0.4;-0.1;0];
B = [0.5;-0.1;0];
C = [0.5;0.1;0];
D = [0.4;0.1;0];
features_world = [A,B,C,D]; %Coordinate punti in terna mondo

%% Desired
q0=[0.1257, 1.2566,-0.8168,2.0106,-0.4398,0];
jaco2.plot(q0,'workspace',[-1 1 -1 1 -0.5 1],'noloop'); title('Final desired configuration of jaco2');hold on;grid;
for k = 1:4
    scatter3(features_world(1,k),features_world(2,k),features_world(3,k),'r','filled');
end

T = jaco2.fkine(q0);
Peef_w = T(1:3,4);
Reef_w = T(1:3,1:3);
Pc_w = Peef_w + Reef_w*Pc_eef;
Rc_w = Reef_w*Rc_eef;
scatter3(Pc_w(1),Pc_w(2),Pc_w(3),'b','filled');

desired_features = [];
f_pixel = [];
for k=1:4
    features_camera(:,k) = inv(Rc_w)*(features_world(:,k) - Pc_w);      %Punti in terna camera
    features_2D(:,k) = features_camera(1:2,k)/features_camera(3,k);     %Proiezione punti
    desired_features = [desired_features; features_2D(1,k); features_2D(2,k)];
    f_pixel = [f_pixel;round(features_2D(1,k)*fx+ppx);round(features_2D(2,k)*fy+ppy)];
end
grid;
%% Starting features
q1 = [0.3770, 1.5079, -0.8168, 2.1991, -0.4398, 0];
figure;
jaco2.plot(q1,'workspace',[-1 1 -1 1 -0.5 1],'noloop'); title('Starting configuration of jaco2');hold on;grid;
for k = 1:4
    scatter3(features_world(1,k),features_world(2,k),features_world(3,k),'r', 'filled');
end
T = jaco2.fkine(q1);
Peef_w = T(1:3,4);
Reef_w = T(1:3,1:3);
Pc_w = Peef_w + Reef_w*Pc_eef;
Rc_w = Reef_w*Rc_eef;
scatter3(Pc_w(1),Pc_w(2),Pc_w(3),'b','filled');
actual_features = [];
act_pixel = [];
for k=1:4
    features_camera_start(:,k) = inv(Rc_w)*(features_world(:,k) - Pc_w);
    features_2D_start(:,k) = features_camera_start(1:2,k)/features_camera_start(3,k);
    actual_features = [actual_features; features_2D_start(1,k); features_2D_start(2,k)];
    act_pixel = [act_pixel; round(features_2D_start(1,k)*fx+ppx);round(features_2D_start(2,k)*fy+ppy)];
end
grid;
%% Plot
figure; title('Desired and actual points in the 2D image plane [m]'); hold on;
for k=1:4
    scatter(features_2D(1,k),features_2D(2,k), 'r', 'filled');
    scatter(features_2D_start(1,k),features_2D_start(2,k), 'g', 'filled');
end
legend('Desired','Actual');grid;
figure; title('Desired and actual points in the 2D image plane [pixel]'); hold on;
for k=0:3
    scatter(f_pixel(2*k+1),f_pixel(2*k+2), 'r', 'filled');
    scatter(act_pixel(2*k+1),act_pixel(2*k+2), 'g', 'filled');
end
legend('Desired','Actual');grid;
%% VS
t0 = 0;
tf = 20;
dt = 0.01;
Z = 0.5;
time = [];
error = [];
vel_end_effector = [];
for t=t0:dt:tf
    time = [time,t];
    e = actual_features - desired_features;
    error = [error,e];
    Ls=[];
    for i=1:4
        x = actual_features(2*i-1);
        y = actual_features(2*i);
        Ls = [Ls;   -1/Z,     0,      x/Z,      x*y,     -(1 + x^2),       y;
            0,     -1/Z,    y/Z,      1 + y^2,     -x*y,         -x];
    end
    vcam = -0.7*pinv(Ls)*e;
    vc_w = [Rc_w,zeros(3);zeros(3),Rc_w]*vcam;
    veef(4:6) = vc_w(4:6);
    if isrow(veef)
        veef = veef';
    end
    veef(1:3) = vc_w(1:3) - cross(veef(4:6),Reef_w*Pc_eef);
    vel_end_effector = [vel_end_effector, veef];
    J = jaco2.jacob0(q1);
    q_dot = inv(J)*veef;
    q = q_dot * dt;
    q1 = ((q1)' + q)';
    T = jaco2.fkine(q1);
    Peef_w = T(1:3,4);
    Reef_w = T(1:3,1:3);
    Pc_w = Peef_w + Reef_w*Pc_eef;
    Rc_w = Reef_w*Rc_eef;
    for k=1:4
        f(:,k) = inv(Rc_w)*(features_world(:,k) - Pc_w);
        f_2D(:,k) = f(1:2,k)/f(3,k);
        actual_features(2*k-1) = f_2D(1,k);
        actual_features(2*k) = f_2D(2,k);
    end
end

figure; title('Errors');hold on;
plot(time, error(1,:)); plot(time, error(2,:));
plot(time, error(3,:)); plot(time, error(4,:));
plot(time, error(5,:)); plot(time, error(6,:));
plot(time, error(7,:)); plot(time, error(8,:));
legend('Epoint1_X','Epoint1_Y','Epoint2_X','Epoint2_Y','Epoint3_X','Epoint3_Y','Epoint4_X','Epoint4_Y');
grid;
figure; title('Features in 2D image plane after VS [m]'); hold on;
for k=0:3
    scatter(desired_features(2*k+1),desired_features(2*k+2), 'r', 'filled');
    scatter(actual_features(2*k+1),actual_features(2*k+2), 'g', 'filled');
end
legend('Desired','Actual');grid;
figure; title('Velocities end effector');hold on;
plot(time, vel_end_effector(1,:)); plot(time, vel_end_effector(2,:));
plot(time, vel_end_effector(3,:)); plot(time, vel_end_effector(4,:));
plot(time, vel_end_effector(5,:)); plot(time, vel_end_effector(6,:));
legend('Vx','Vy','Vz','Wx','Wy','Wz');
grid;