function robot=jaco2_definition()

%% Robot definition
% DH Parameter
D1=0.2755;
D2=0.4100;
D3=0.2073;
D4=0.0741;
D5=0.0741;
D6=0.1600;
e2=0.0098;
aa=30.0*pi/180;
ca=cos(aa);
sa=sin(aa);
c2a=cos(2*aa);
s2a=sin(2*aa);
d4b=D3+sa/s2a*D4;
d5b=sa/s2a*D4+sa/s2a*D5;
d6b=sa/s2a*D5+D6;

% Definition of Link 1
Link_1 = Link('alpha',pi/2,'a',0,'d',D1);
Link_1.m=0.748;
Link_1.I=[1.02e-2 -5.21e-7 -2.12e-3;
         -5.21e-7 3.25e-2 -4.14e-7;
         -2.12e-3 -4.14e-7 3.54e-2];
Link_1.G=136;   %%
Link_1.Jm=0;    %%

% Definition of Link 2
Link_2=Link('alpha',pi,'a',D2,'d',0);
Link_2.m=1.11;
Link_2.I=[3.79e-1 0.000 0.000;
          0.000 3.73e-1 -5.90e-6;
          0.000 -5.90e-6 6.98e-3];
Link_2.G=160;   %%
Link_2.Jm=0;    %%

% Definition of Link 3
Link_3=Link('alpha',pi/2,'a',0,'d',-e2);
Link_3.m=0.568;
Link_3.I=[5.11e-2 1.92e-7 -2.67e-7;
          1.92e-7 1.44e-1 -9.59e-5;
         -2.67e-7 -9.59e-5 1.29e-2];
Link_3.G=136;   %%
Link_3.Jm=0;    %%

% Definition of Link 4
Link_4=Link('alpha',2*aa,'a',0,'d',-d4b);
Link_4.m=0.427;
Link_4.I=[2.46e-3 8.33e-4 3.35e-8;
          8.33e-4 3.42e-3 3.40e-8;
          3.35e-8 3.40e-8 3.63e-3];
Link_4.G=110;   %%
Link_4.Jm=0;    %%

% Definition of Link 5
Link_5=Link('alpha',2*aa,'a',0,'d',-d5b);
Link_5.m=0.427;
Link_5.I=[2.46e-3 8.33e-4 3.35e-8;
          8.33e-4 3.42e-3 3.40e-8;
          3.35e-8 3.40e-8 3.63e-3];
Link_5.G=110;   %%
Link_5.Jm=0;    %%

% Definition of Link hand
Link_hand=Link('alpha',pi,'a',0,'d',-d6b);
Link_hand.m=0.727;
Link_hand.I=[4.66e-3 4.13e-4 -1.05e-5;
            4.13e-4 2.65e-3 -3.24e-6;
           -1.05e-5 -3.24e-6 5.25e-3];
Link_hand.G=110;   %%
Link_hand.Jm=0;    %%

robot = SerialLink ([Link_1 Link_2 Link_3 Link_4 Link_5 Link_hand])
robot.name='JACO2';
