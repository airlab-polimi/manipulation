function z = computeZ(Pee_w,Ree_w,Pdepl_w)
    Ree_w = [Ree_w(1:3),Ree_w(4:6),Ree_w(7:9)];
    offset = [0;0;0];
    Pdepl_w = Pdepl_w + offset;
    Pdepl_ee = Ree_w\(Pdepl_w - Pee_w);
z = Pdepl_ee(3);

