function features = computeFeatures(Pc_w,Rc_w,Pdepl_w,camera_param)
    Rc_w = [Rc_w(1:3),Rc_w(4:6),Rc_w(7:9)];
    features_camera_frame = Rc_w\(Pdepl_w - Pc_w);
    features_2D = features_camera_frame(1:2)/features_camera_frame(3);
    actual_features = [round(features_2D(1)*camera_param.fx+camera_param.ppx);round(features_2D(2)*camera_param.fy+camera_param.ppy)];
features = actual_features;

