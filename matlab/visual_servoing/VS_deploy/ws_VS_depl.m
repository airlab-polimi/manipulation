clear all
close all

%% Setup
startup_rvc

% Camera parameters
camera_param.fx    = 309.341;
camera_param.fy    = 312.294;
camera_param.ppx   = 155.386;
camera_param.ppy   = 120.498;
camera_param.Pc_ee = [0;0.03;-0.1];
camera_param.Rc_ee = eul2rotm([0,0,0]);

% Robot parameters
jaco2 = jaco2_definition();

% Release point (world)
Pdepl_w = [0.4; -0.05; 0.6];

% Dispenser center in pixel coordinate (constant)
desired_features = [160;160];

%% Initial conditions
q0 = [-0.1885, 1.8849, -1.0681, 1.6336, -0.1256, 0];
T = jaco2.fkine(q0);
% Pee_w = T(1:3,4);         % matlab 2016a
% Ree_w = T(1:3,1:3);       % matlab 2016a
Pee_w = T.t;                % matlab 2017a
Ree_w = [T.n, T.o, T.a];    % matlab 2017a
Pc_w = Pee_w + Ree_w*camera_param.Pc_ee;
Rc_w = Ree_w*camera_param.Rc_ee;

% Plot of robot, camera and depl. point
% jaco2.plot(q0,'workspace',[-1 1 -1 1 -0.1 1.5]); title('Final desired configuration of jaco2'); hold on;
% scatter3(Pc_w(1),Pc_w(2),Pc_w(3),'b','filled'); scatter3(Pdepl_w(1),Pdepl_w(2),Pdepl_w(3),'b','filled');

Z_Ls = - camera_param.Pc_ee(3);
gain_xy = 0.8;
gain_z = 0.4;