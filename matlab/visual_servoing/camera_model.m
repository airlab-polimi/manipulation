function features = camera_model(Tee_world_vect,marker_world,camera_parameters)

Pee_w = Tee_world_vect(13:15);
Ree_w = [Tee_world_vect(1:3), Tee_world_vect(5:7), Tee_world_vect(9:11)];

fx  = camera_parameters.fx;
fy  = camera_parameters.fy;
ppx = camera_parameters.ppx;
ppy = camera_parameters.ppy;
  
Pc_world = Pee_w+Ree_w*camera_parameters.Pc_ee;
Rc_world = Ree_w*camera_parameters.Rc_ee;
  
features = zeros(8,1);
for k=1:4
    marker_camera = Rc_world'*(marker_world(3*k-2:3*k) - Pc_world);
    marker_2D = marker_camera(1:2)/marker_camera(3);
    features(2*k-1:2*k,1) = [round(marker_2D(1)*fx+ppx);round(marker_2D(2)*fy+ppy)];
end
