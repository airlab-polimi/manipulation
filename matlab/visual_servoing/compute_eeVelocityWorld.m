function Vee_world = compute_eeVelocityWorld(Vcam_camera, Tee_world_vect, camera_parameters)

Pc_ee = camera_parameters.Pc_ee;
Rc_ee = camera_parameters.Rc_ee;

Ree_world = [Tee_world_vect(1:3), Tee_world_vect(5:7), Tee_world_vect(9:11)];

% Compute end effector velocity
Vee_world = zeros(6,1);
Vee_world(4:6) = Ree_world*Rc_ee*Vcam_camera(4:6);
Vee_world(1:3) = Ree_world*Rc_ee*Vcam_camera(1:3)-cross(Vee_world(4:6),Ree_world*Pc_ee);