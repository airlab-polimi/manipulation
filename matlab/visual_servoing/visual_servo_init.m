clear all
close all

%% Setup
startup_rvc

% Camera parameters
camera_param.fx    = 309.341;
camera_param.fy    = 312.294;
camera_param.ppx   = 155.386;
camera_param.ppy   = 120.498;
camera_param.Pc_ee = [-0.0823, 0.0675, -0.1375]';
camera_param.Rc_ee = [-0.9538, -0.2864, -0.0751; 0.2937, -0.9491, -0.0084; -0.0693, -0.0302, 0.9955];

% Robot parameters
jaco2 = jaco2_definition();

%% Marker position (world)
A = [ 0.07; -0.55; -0.06];
B = [ 0.07; -0.59; -0.06];
C = [ 0.03; -0.59; -0.06];
D = [ 0.03; -0.55; -0.06];
marker_world = [A; B; C; D];

%% Desired features
q0 = jointpos_jaco2dh([254.55090332, 232.967391968, 83.7159194946, 352.234222412, 40.4178962708, 417.822845459])';   % Desired robot position in joint coordinates
T = jaco2.fkine(q0);                                % Desired cartesian robot pose
Pee_w = T(1:3,4);
Ree_w = T(1:3,1:3);
Pc_w = Pee_w + Ree_w*camera_param.Pc_ee;            % Desired camera pose
Rc_w = Ree_w*camera_param.Rc_ee;

desired_features_pixel = [];
for k=1:4
    marker_camera(:,k) = Rc_w'*(marker_world(3*k-2:3*k) - Pc_w);
    marker_camera_2D(:,k) = marker_camera(1:2,k)/marker_camera(3,k);
    desired_features_pixel = [desired_features_pixel;
        round(marker_camera_2D(1,k)*camera_param.fx+camera_param.ppx); 
        round(marker_camera_2D(2,k)*camera_param.fy+camera_param.ppy)];
end

%% Initial values
q1 = jointpos_jaco2dh([259.18371582, 200.893463135, 114.474319458, 319.419464111, 122.725845337, 390.318878174])'; % Initial robot position in joint coordinate
T = jaco2.fkine(q1);                                % Initial cartesian robot pose
Pee_w = T(1:3,4);
Ree_w = T(1:3,1:3);
Pc_w = Pee_w + Ree_w*camera_param.Pc_ee;            % Initial camera pose
Rc_w = Ree_w*camera_param.Rc_ee;

initial_features_pixel = [];
for k=1:4
    marker_camera(:,k) = Rc_w'*(marker_world(3*k-2:3*k) - Pc_w);
    marker_camera_2D(:,k) = marker_camera(1:2,k)/marker_camera(3,k);
    initial_features_pixel = [initial_features_pixel;
        round(marker_camera_2D(1,k)*camera_param.fx+camera_param.ppx); 
        round(marker_camera_2D(2,k)*camera_param.fy+camera_param.ppy)];
end

%% Control parameters
Z_Ls = 0.45;     % Depth for image jacobian computation
vs_gain = 0.5;  % Visual servoing controller gain
