clear all
close all
%% Camera parameters
fx = 309.341;
fy = 312.294;
ppx = 155.386;
ppy = 120.498;
%% Desired
A = [0.1;0.1;0];
B = [0.2;0.1;0];
C = [0.2;0.2;0];
D = [0.1;0.2;0];
features_world = [A,B,C,D]; %Coordinate punti in terna mondo
Pc_w = [0.15;0.15;0.5];     %Traslazione camera rispetto all'origine
rot = [0,pi,0];
Rc_w = eul2rotm(rot);       %Rotazione camera rispetto all'origine
figure; title('Desired and actual points in the 2D image plane [m]'); hold on;
desired_features = [];
for k=1:4
    features_camera(:,k) = inv(Rc_w)*(features_world(:,k) - Pc_w);      %Punti in terna camera
    features_2D(:,k) = features_camera(1:2,k)/features_camera(3,k);     %Proiezione punti
    string = int2str(k);
    scatter(features_2D(1,k),features_2D(2,k), 'g', 'filled'); text(features_2D(1,k),features_2D(2,k), string);
    desired_features = [desired_features; features_2D(1,k); features_2D(2,k)];
end
grid;
%% Actual
Pc_w = [0.25;0.22;0.8];         % Posizione camera di partenza
rot = [0.1,pi,0.2];
Rc_w = eul2rotm(rot);
actual_features = [];
for k=1:4
    features_camera_start(:,k) = inv(Rc_w)*(features_world(:,k) - Pc_w);
    features_2D_start(:,k) = features_camera_start(1:2,k)/features_camera_start(3,k);
    string = int2str(k);
    scatter(features_2D_start(1,k),features_2D_start(2,k), 'r', 'filled'); text(features_2D_start(1,k),features_2D_start(2,k), string);
    actual_features = [actual_features; features_2D_start(1,k); features_2D_start(2,k)];
end
%% VS
t0 = 0;
tf = 20;
dt = 0.01;
Z = 0.5;
time = [];
error = [];
for t=t0:dt:tf
    time = [time,t];
    e = actual_features - desired_features;
    error = [error,e];
    Ls=[];
    for i=1:4
        x = actual_features(2*i-1);
        y = actual_features(2*i);
        Ls = [Ls;   -1/Z,     0,      x/Z,      x*y,     -(1 + x^2),       y;
            0,     -1/Z,    y/Z,      1 + y^2,    -x*y,         -x];
    end
    vcam = -0.4*pinv(Ls)*e;
    s_dot = Ls*vcam;
    delta_s = s_dot*dt;
    actual_features = actual_features+delta_s;
end
figure; hold on;
plot(time, error(1,:)); plot(time, error(2,:));
plot(time, error(3,:)); plot(time, error(4,:));
plot(time, error(5,:)); plot(time, error(6,:));
plot(time, error(7,:)); plot(time, error(8,:));
grid;
figure; title('Features in 2D image plane after VS [m]'); hold on;
for k=0:3
    scatter(desired_features(2*k+1),desired_features(2*k+2), 'r', 'filled');
    scatter(actual_features(2*k+1),actual_features(2*k+2), 'g', 'filled');
end
grid;