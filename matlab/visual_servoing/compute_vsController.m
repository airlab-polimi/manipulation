function vcamera = compute_vsController(actual_features, Z, desired_features, camera_parameters, vs_gain)

Ls = compute_imageJacobian(actual_features, Z, camera_parameters);

% Compute visual servoing control law
vcamera = -vs_gain*inv(Ls'*Ls)*Ls'*(actual_features-desired_features);
