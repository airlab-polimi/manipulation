function jointpos_jaco = jointpos_dh2jaco(jointpos_DH)

q1 = -jointpos_DH(1)*180/pi;
q2 = jointpos_DH(2)*180/pi + 90;
q3 = jointpos_DH(3)*180/pi - 90;
q4 = jointpos_DH(4)*180/pi;
q5 = jointpos_DH(5)*180/pi + 180;
q6 = jointpos_DH(6)*180/pi - 90;

jointpos_jaco = [q1, q2, q3, q4, q5, q6]';