function Ls = compute_imageJacobian(actual_features, Z, camera_parameters)

fx  = camera_parameters.fx;
fy  = camera_parameters.fy;
ppx = camera_parameters.ppx;
ppy = camera_parameters.ppy;
    
% Compute image Jacobian
Ls = zeros(8,6);
for i=1:4
    u = actual_features(2*i-1)-ppx;
    v = actual_features(2*i)-ppy;
    Ls(2*i-1:2*i, :) = [-fx/Z,     0, u/Z,    u*v/fy, -(fx+u^2/fx),  v*fx/fy;
                            0, -fy/Z, v/Z, fy+v^2/fy,      -u*v/fx, -u*fy/fx];
end
