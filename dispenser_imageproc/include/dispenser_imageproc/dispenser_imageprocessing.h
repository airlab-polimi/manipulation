#ifndef DISPENSER_IMAGEPROCESSING_H_
#define DISPENSER_IMAGEPROCESSING_H_

#include "ros/ros.h"
#include "geometry_msgs/Point.h"
#include "dispenser_imageproc/imagePlane2cameraFrame.h"
#include "dispenser_imageproc/cameraFrame2imagePlane.h"
#include "dispenser_imageproc/getCameraIntrinsicParam.h"
#include "dispenser_imageproc/validateDeployment.h"
#include "dispenser_imageproc/validateGrasping.h"

#include "std_msgs/Bool.h"

#include <librealsense/rs.hpp>
#include <eigen3/Eigen/Dense>
#include <opencv2/opencv.hpp>
#include <opencv2/aruco.hpp>

#define DEBUG

#define RUN_PERIOD_DEFAULT 0.02
/* Used only if the actual value of the period is not retrieved from the ROS parameter server */

#define NAME_OF_THIS_NODE "dispenser_imageproc"


class dispenser_imageprocessing
{
  private:
    ros::NodeHandle Handle;

    enum processingState_type {IDLE=0, GRASPING=1, DEPLOYING=2};

    /* ROS topics */
    ros::Publisher _aruco0_markerFeatures_publisher, _aruco1_markerFeatures_publisher, _aruco2_markerFeatures_publisher, _aruco3_markerFeatures_publisher, _aruco4_markerFeatures_publisher;
    ros::Publisher _aruco0_markerCorners_publisher, _aruco1_markerCorners_publisher, _aruco2_markerCorners_publisher, _aruco3_markerCorners_publisher, _aruco4_markerCorners_publisher;
    ros::Publisher _dispenserCenterCamera_publisher;

    ros::Publisher _deploymentPoint_imagePlane_publisher;
    ros::Subscriber _deploymentPoint_cameraframe_subscriber;

    ros::Publisher _checkDeploy_publisher, _checkGrasp_publisher;
 
    /* ROS services */
    ros::ServiceServer _cameraIntrinsicParam_srv;
    ros::ServiceServer _imagePlane2cameraFrame_srv;
    ros::ServiceServer _cameraFrame2imagePlane_srv;
    ros::ServiceServer _validate_grasp_srv;
    ros::ServiceServer _validate_deployment_srv;

    /* Node parameters */
    std::string _window_debug_marker, _window_debug_dispenser, _window_debug_deployment;
    int _frame_width, _frame_height, _framerate;
    double _camera_min_depth, _camera_max_depth;
    double _aruco_winsize_min, _aruco_winsize_max, _aruco_winsize_step;
    double _dispenser_roi_xmin, _dispenser_roi_xmax, _dispenser_roi_ymin, _dispenser_roi_ymax;
    double _dispenser_area_min, _dispenser_area_max, _dispenser_points_min;
    double _check_grasp_roi_xmin, _check_grasp_roi_ymin, _check_grasp_area_min, _check_grasp_area_max;
    double _check_deploy_roi_xmin, _check_deploy_roi_ymin, _check_deploy_area_min, _check_deploy_area_max, _check_deploy_depth_max;
    double _match_shapes_threshold, _grasp_threshold;
    double _floodfill_low, _floodfill_high, _floodfill_neighbours;
    int _distance_max;
    std::vector<int> _hsv_thsd_min, _hsv_thsd_max;

    // data structures and callbacks for the validation of grasping and deployment operations
    int numMeasurementForValidation;
    bool validateDeployment(dispenser_imageproc::validateDeployment::Request &req, dispenser_imageproc::validateDeployment::Response &res);
    bool validateGrasping(dispenser_imageproc::validateGrasping::Request &req, dispenser_imageproc::validateGrasping::Response &res);
    std::list<bool> checkGraspData;
    std::list<bool> checkDeploymentData;

    /* Controller periodic task */
    void PeriodicTask(void);

    /* Camera variables */
    rs::context     _rs_ctx;
    rs::device*     _rs_camera;
    rs::intrinsics  _depth_intrin;
    rs::intrinsics  _color_intrin;
    float           _scaleFromDepth2Color;
    enum acquisition_type {COLOR, DEPTH, ALL};

    /* Image variables */
    const uint16_t* _depth_image;
    cv::Mat         _BGR_image;
#ifdef DEBUG
    cv::Mat         _debug_image_dispenser;
    cv::Mat         _debug_image_marker;
    cv::Mat         _debug_image_deploy;
#endif

    std::mutex deploymentPoint_mutex;
    cv::Point3d _actual_deploymentPoint_cameraFrame;
    
    /* Aruco marker variables */
    cv::Ptr<cv::aruco::Dictionary>          _aruco_dictionary;
    cv::Ptr<cv::aruco::DetectorParameters>  _aruco_detector;

    /* Realsense camera functions */
    bool realsensecamera_init();
    void realsensecamera_acquire(acquisition_type acq_type);
    void realsensecamera_imagePlane2cameraFrame(cv::Point p_image, float p_depth, cv::Point3d& P_camera, rs::intrinsics intrinsic_param);
    void realsensecamera_cameraFrame2imagePlane(cv::Point& p_image, cv::Point3d P_camera, rs::intrinsics intrinsic_param);
    bool realsensecamera_findDepth(cv::Point p_image, float& p_depth);

    void deploymentPoint_MessageCallback(const geometry_msgs::Point& deploymentPoint);

    /* Image processing functions */
    bool find_aruco_marker(std::vector<int>& marker_ids, std::vector<std::vector<cv::Point2f>>& corner_features, std::vector<std::vector<cv::Point3d>>& corner_camera);
    void detectContours(std::vector<std::vector<cv::Point>>& contours);
    bool selectContours(std::vector<std::vector<cv::Point>>& contours, int x_min, int x_max, int y_min, int y_max, float area_min, float area_max);
    bool find_dispenser_center(std::vector<std::vector<cv::Point>>& contours, cv::Point3d& dispenser_center_camera);
    bool check_deploy(std::vector<std::vector<cv::Point>>& contours);
    bool check_grasp(std::vector<std::vector<cv::Point>>& contours);

    bool find_deployment_point(cv::Point Pdepl, cv::Point& deployment_point_image_plane);
    
    /* Node services callbacks */
    bool imagePlane2cameraFrame(dispenser_imageproc::imagePlane2cameraFrame::Request &req, dispenser_imageproc::imagePlane2cameraFrame::Response &res);
    bool cameraFrame2imagePlane(dispenser_imageproc::cameraFrame2imagePlane::Request &req, dispenser_imageproc::cameraFrame2imagePlane::Response &res);
    bool getCameraIntrinsicParam(dispenser_imageproc::getCameraIntrinsicParam::Request &req, dispenser_imageproc::getCameraIntrinsicParam::Response &res);

    /* Helpers */
    void getParametersFromYaml();
  public:

    double RunPeriod;
    void Prepare(void);
    void RunPeriodically(float Period);
    void Shutdown(void);
};

#endif /* DISPENSER_IMAGEPROCESSING_H_ */
