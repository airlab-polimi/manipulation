#ifndef POINT_CLOUD_ANALYSIS_H_
#define POINT_CLOUD_ANALYSIS_H_

#include "/opt/ros/kinetic/include/librealsense/rs.hpp"
#include "/opt/ros/kinetic/include/pcl_ros/point_cloud.h"
#include <pcl/visualization/cloud_viewer.h>
#include <eigen3/Eigen/Dense>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Pose.h"
#include <ros/ros.h>
#include "std_msgs/Float64.h"
#include <math.h>
#include <vector>
#include <opencv2/opencv.hpp>
#include <boost/make_shared.hpp>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/filter.h>
#include <pcl/features/normal_3d.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transforms.h>

//-------------------------------------------------------------------
//  Helper class required by PCL
//-------------------------------------------------------------------

class NewPointRepresentation : public pcl::PointRepresentation <pcl::PointNormal>
{
  using pcl::PointRepresentation<pcl::PointNormal>::nr_dimensions_;
  public:
    NewPointRepresentation ()
    {
      // Define the number of dimensions
      nr_dimensions_ = 4;
    }

    // Override the copyToFloatArray method to define our feature vector
    virtual void copyToFloatArray (const pcl::PointNormal &p, float * out) const
    {
      // < x, y, z, curvature >
      out[0] = p.x;
      out[1] = p.y;
      out[2] = p.z;
      out[3] = p.curvature;
    }
};

using namespace rs;

class point_cloud_analysis {


//---------------------------------

  Eigen::Vector3f Pc_eef;
  Eigen::Matrix3f Rc_eef;
  Eigen::Matrix3f Reef_o;
  Eigen::Vector3f Peef_o;
  
  Eigen::Vector3f Pdepl_w;
  
  rs::context _rs_ctx;
  rs::device* _rs_camera;
  int const INPUT_WIDTH = 320;
  int const INPUT_HEIGHT = 240;
  int const FRAMERATE = 60;
  
  geometry_msgs::Point centroid_world_frame;
  geometry_msgs::Pose desired_pose;
  
  rs::intrinsics  _depth_intrin;
  rs::intrinsics  _color_intrin;
  rs::extrinsics _depth_to_color;
  float _scaleFromDepth2Color;
  
  const int NUMBER_OF_POSES = 3;
  int count_pose;
  
  pcl::visualization::CloudViewer viewer;
  std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr, Eigen::aligned_allocator<pcl::PointCloud<pcl::PointXYZ>::Ptr> > sourceClouds;
  
  pcl::PointXYZ p1;
  pcl::PointXYZ p2;
  pcl::PointXYZ P;
  
  pcl::PointXYZ centroid;
  
  cv::Mat _BGR_image;

//---------------------------------

  // principal component analysis 
  void pca(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);
  void viewerOneOff (pcl::visualization::PCLVisualizer& v);
  void record_point_cloud();
  /*
  It is the actual pair registration; the last paramater states if we have to down sample our clouds, useful in the case of large datasets
  */
  void pairAlign (const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_src, const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_tgt, pcl::PointCloud<pcl::PointXYZ>::Ptr output, Eigen::Matrix4f &final_transform, bool downsample);
  /*
  The algorithm is used in order to incrementally register a series of point clouds two by two.
  The idea is to transform all the clouds in a common frame: the best transform between each consecutive cloud is found, and these transforms are accumulated over the whole set of clouds
  */
  void iterative_closest_point_algo(pcl::PointCloud<pcl::PointXYZ>::Ptr result);

public:
  void init(rs::device* dev);
  point_cloud_analysis();
  // set pose of the robot, from which the current snap os taken
  void setRobotPose(geometry_msgs::Pose pose);

  // set coordinate of the deployment point estimated by pointClouduilder
  // this is a candidate deployment point, the goal is to find the best point around this one
  void setEstimatedDeploymentTarget(geometry_msgs::Point estimatedTarget);

  // core loop, returns centroid of deployment position and required arm pose
  std::pair<geometry_msgs::Pose, geometry_msgs::Point> core(bool isLastSnap);

};

#endif