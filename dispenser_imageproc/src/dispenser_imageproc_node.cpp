#include "dispenser_imageproc/dispenser_imageprocessing.h"


int main(int argc, char **argv)
{
    ros::init(argc, argv, NAME_OF_THIS_NODE);
  
    dispenser_imageprocessing dispenser_imageproc_node;
  
    dispenser_imageproc_node.Prepare();
  
    dispenser_imageproc_node.RunPeriodically(dispenser_imageproc_node.RunPeriod);
  
    dispenser_imageproc_node.Shutdown();
  
    return (0);
}
