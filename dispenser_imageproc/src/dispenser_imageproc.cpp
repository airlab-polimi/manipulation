#include "dispenser_imageproc/dispenser_imageprocessing.h"

#include "vs_msgs/featurePoint.h"
#include "vs_msgs/featurePointArray.h"
#include "vs_msgs/cameraPoint.h"
#include "vs_msgs/cameraPointArray.h"

void dispenser_imageprocessing::getParametersFromYaml(){
  /* Retrieve parameters from ROS parameter server */
  std::string FullParamName;

  FullParamName = ros::this_node::getName()+"/run_period";
  if (false == Handle.getParam(FullParamName, RunPeriod))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/window_debug_marker";
  if (false == Handle.getParam(FullParamName, _window_debug_marker))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/window_debug_dispenser";
  if (false == Handle.getParam(FullParamName, _window_debug_dispenser))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/window_debug_deployment";
  if (false == Handle.getParam(FullParamName, _window_debug_deployment))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/image_width";
  if (false == Handle.getParam(FullParamName, _frame_width))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/image_height";
  if (false == Handle.getParam(FullParamName, _frame_height))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/framerate";
  if (false == Handle.getParam(FullParamName, _framerate))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/min_depth";
  if (false == Handle.getParam(FullParamName, _camera_min_depth))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/max_depth";
  if (false == Handle.getParam(FullParamName, _camera_max_depth))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/aruco_winsize_min";
  if (false == Handle.getParam(FullParamName, _aruco_winsize_min))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/aruco_winsize_max";
  if (false == Handle.getParam(FullParamName, _aruco_winsize_max))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/aruco_winsize_step";
  if (false == Handle.getParam(FullParamName, _aruco_winsize_step))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/dispenser_roi_xmin";
  if (false == Handle.getParam(FullParamName, _dispenser_roi_xmin))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/dispenser_roi_xmax";
  if (false == Handle.getParam(FullParamName, _dispenser_roi_xmax))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/dispenser_roi_ymin";
  if (false == Handle.getParam(FullParamName, _dispenser_roi_ymin))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/dispenser_roi_ymax";
  if (false == Handle.getParam(FullParamName, _dispenser_roi_ymax))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/dispenser_area_min";
  if (false == Handle.getParam(FullParamName, _dispenser_area_min))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/dispenser_area_max";
  if (false == Handle.getParam(FullParamName, _dispenser_area_max))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/hsv_threshold_min";
  if (false == Handle.getParam(FullParamName, _hsv_thsd_min))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/hsv_threshold_max";
  if (false == Handle.getParam(FullParamName, _hsv_thsd_max))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/num_measurement_for_validation";
  if (false == Handle.getParam(FullParamName, numMeasurementForValidation))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/check_grasp_roi_xmin";
  if (false == Handle.getParam(FullParamName, _check_grasp_roi_xmin))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/check_grasp_roi_ymin";
  if (false == Handle.getParam(FullParamName, _check_grasp_roi_ymin))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/check_grasp_area_min";
  if (false == Handle.getParam(FullParamName, _check_grasp_area_min))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/check_grasp_area_max";
  if (false == Handle.getParam(FullParamName, _check_grasp_area_max))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/check_deploy_roi_xmin";
  if (false == Handle.getParam(FullParamName, _check_deploy_roi_xmin))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/check_deploy_roi_ymin";
  if (false == Handle.getParam(FullParamName, _check_deploy_roi_ymin))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/check_deploy_area_min";
  if (false == Handle.getParam(FullParamName, _check_deploy_area_min))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/check_deploy_area_max";
  if (false == Handle.getParam(FullParamName, _check_deploy_area_max))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/check_deploy_depth_max";
  if (false == Handle.getParam(FullParamName, _check_deploy_depth_max))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/match_shapes_threshold";
  if (false == Handle.getParam(FullParamName, _match_shapes_threshold))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/grasp_threshold";
  if (false == Handle.getParam(FullParamName, _grasp_threshold))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/floodfill_low";
  if (false == Handle.getParam(FullParamName, _floodfill_low))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/floodfill_high";
  if (false == Handle.getParam(FullParamName, _floodfill_high))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/floodfill_neighbours";
  if (false == Handle.getParam(FullParamName, _floodfill_neighbours))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  FullParamName = ros::this_node::getName()+"/distance_max";
  if (false == Handle.getParam(FullParamName, _distance_max))
     ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

  _actual_deploymentPoint_cameraFrame.x = _actual_deploymentPoint_cameraFrame.y = _actual_deploymentPoint_cameraFrame.z = std::nan("");
}


void dispenser_imageprocessing::Prepare(void)
{
    RunPeriod = RUN_PERIOD_DEFAULT;
    getParametersFromYaml();
  
    /* ROS topics */
    _aruco0_markerFeatures_publisher = Handle.advertise<vs_msgs::featurePointArray>("/dispenser_imgproc/aruco0_featurePoints", 1);
    _aruco0_markerCorners_publisher  = Handle.advertise<vs_msgs::cameraPointArray>("/dispenser_imgproc/aruco0_cornersCamera", 1);
    _aruco1_markerFeatures_publisher = Handle.advertise<vs_msgs::featurePointArray>("/dispenser_imgproc/aruco1_featurePoints", 1);
    _aruco1_markerCorners_publisher  = Handle.advertise<vs_msgs::cameraPointArray>("/dispenser_imgproc/aruco1_cornersCamera", 1);
    _aruco2_markerFeatures_publisher = Handle.advertise<vs_msgs::featurePointArray>("/dispenser_imgproc/aruco2_featurePoints", 1);
    _aruco2_markerCorners_publisher  = Handle.advertise<vs_msgs::cameraPointArray>("/dispenser_imgproc/aruco2_cornersCamera", 1);
    _aruco3_markerFeatures_publisher = Handle.advertise<vs_msgs::featurePointArray>("/dispenser_imgproc/aruco3_featurePoints", 1);
    _aruco3_markerCorners_publisher  = Handle.advertise<vs_msgs::cameraPointArray>("/dispenser_imgproc/aruco3_cornersCamera", 1);
    _aruco4_markerFeatures_publisher = Handle.advertise<vs_msgs::featurePointArray>("/dispenser_imgproc/aruco4_featurePoints", 1);
    _aruco4_markerCorners_publisher  = Handle.advertise<vs_msgs::cameraPointArray>("/dispenser_imgproc/aruco4_cornersCamera", 1);
    _dispenserCenterCamera_publisher = Handle.advertise<geometry_msgs::Point>("/dispenser_imgproc/dispenser_centerCamera", 1);

    _deploymentPoint_imagePlane_publisher = Handle.advertise<vs_msgs::featurePoint>("/dispenser_imgproc/deploymentPoint_imagePlane", 1);
    _deploymentPoint_cameraframe_subscriber = Handle.subscribe("/grasping_controller/deployment_point_camera_frame", 1, &dispenser_imageprocessing::deploymentPoint_MessageCallback, this);

    _checkGrasp_publisher = Handle.advertise<std_msgs::Bool>("/dispenser_imgproc/check_grasp", 1);
    _checkDeploy_publisher = Handle.advertise<std_msgs::Bool>("/dispenser_imgproc/check_deploy", 1);

    /* ROS services */
    _cameraIntrinsicParam_srv   = Handle.advertiseService("getCameraIntrinsicParam", &dispenser_imageprocessing::getCameraIntrinsicParam, this);
    _cameraFrame2imagePlane_srv = Handle.advertiseService("cameraFrame2imagePlane", &dispenser_imageprocessing::cameraFrame2imagePlane, this);
    _imagePlane2cameraFrame_srv = Handle.advertiseService("imagePlane2cameraFrame", &dispenser_imageprocessing::imagePlane2cameraFrame, this);
    _validate_grasp_srv         = Handle.advertiseService("validateGrasping", &dispenser_imageprocessing::validateGrasping, this);
    _validate_deployment_srv    = Handle.advertiseService("validateDeployment", &dispenser_imageprocessing::validateDeployment, this);

    /* Initialize aruco detector */
  	_aruco_dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);

  	_aruco_detector = cv::aruco::DetectorParameters::create();
    _aruco_detector->adaptiveThreshWinSizeMin  = _aruco_winsize_min;
    _aruco_detector->adaptiveThreshWinSizeMax  = _aruco_winsize_max;
    _aruco_detector->adaptiveThreshWinSizeStep = _aruco_winsize_step;

    /* Initialize hardware */
    if (!realsensecamera_init())
    {
      ROS_ERROR("Cannot initialize Realsense camera: connect a camera to the system!");
      Shutdown();
    }

    ROS_INFO("Node %s ready to run.", ros::this_node::getName().c_str());
}

//----------------------------------
// Validation Services callbacks
//----------------------------------


// both of them check if the last numMeasurementForValidation measurement 
// in checkDeploymentData and checkGraspData respectively, detect a correct execution, or not

bool dispenser_imageprocessing::validateDeployment(dispenser_imageproc::validateDeployment::Request &req, dispenser_imageproc::validateDeployment::Response &res){
  int counter = 0;

  std::list<bool>::iterator it;
  for (it = checkDeploymentData.begin(); it != checkDeploymentData.end(); it++) {
    // ROS_INFO("%d", *it);
    if(*it == true)
      counter++;
  }
  res.is_dispenser_deployed = counter >= checkDeploymentData.size()/2 ? true : false;
  return true;
}

bool dispenser_imageprocessing::validateGrasping(dispenser_imageproc::validateGrasping::Request &req, dispenser_imageproc::validateGrasping::Response &res){
  int counter = 0;

  std::list<bool>::iterator it;
  for (it = checkGraspData.begin(); it != checkGraspData.end(); it++) {
    // ROS_INFO("%d", *it);
    if(*it == true)
      counter++;
  }
  res.is_dispenser_grasped = counter >= checkGraspData.size()/2 ? true : false;
  return true;
}



//----------------------------------



void dispenser_imageprocessing::deploymentPoint_MessageCallback(const geometry_msgs::Point& deploymentPoint)
{
  deploymentPoint_mutex.lock();
  _actual_deploymentPoint_cameraFrame.x = deploymentPoint.x;
  _actual_deploymentPoint_cameraFrame.y = deploymentPoint.y;
  _actual_deploymentPoint_cameraFrame.z = deploymentPoint.z;
  deploymentPoint_mutex.unlock();
}

void dispenser_imageprocessing::RunPeriodically(float Period)
{
    ros::Rate LoopRate(1.0/Period);

    ROS_INFO("Node %s running periodically (T=%.2fs, f=%.2fHz).", ros::this_node::getName().c_str(), Period, 1.0/Period);

    while (ros::ok())
    {
	    PeriodicTask();
	    ros::spinOnce();
	    LoopRate.sleep();
    }
}


void dispenser_imageprocessing::Shutdown(void)
{
    ROS_INFO("Node %s shutting down.", ros::this_node::getName().c_str());
}


void dispenser_imageprocessing::PeriodicTask(void)
{
  /* Acquire a frame from the camera */
  realsensecamera_acquire(ALL);

  /* Find aruco marker corners and publish results */
  std::vector<int> marker_ids;
  std::vector<std::vector<cv::Point2f>> corner_features;
  std::vector<std::vector<cv::Point3d>> corner_camera;

  vs_msgs::featurePointArray corner_features_msg;
  vs_msgs::cameraPointArray corners_camera_msg;

  if (find_aruco_marker(marker_ids, corner_features, corner_camera))
  {	
  	/* Iterator for available markers*/
    for (int i = 0; i < 5; i++)
    {
      /* Initialize message variables */
      corner_features_msg.featurePoints.clear();
      corner_features_msg.num_featurePoints = 0;
      corners_camera_msg.cameraPoints.clear();
      corners_camera_msg.num_cameraPoints = 0;

      /* Iterator for detected markers */
      for(int j = 0; j < marker_ids.size(); j++)
      {
		    /* If a marker is found, the corresponding message is built */        
        if(i==marker_ids.at(j))
        {
          corner_features_msg.num_featurePoints = corner_features.at(j).size();

          for (int k = 0; k < corner_features.at(j).size(); k++) {
            vs_msgs::featurePoint corner_feature_msg;
            corner_feature_msg.x = corner_features.at(j).at(k).x;
            corner_feature_msg.y = corner_features.at(j).at(k).y;

            corner_features_msg.featurePoints.push_back(corner_feature_msg);
          }

          /* If all corners of the marker can be converted in camera frame, the message is built, otherwise an empty message is generated */
          if(corner_camera.at(j).size() == 4)
          {
          	corners_camera_msg.num_cameraPoints = corner_camera.at(j).size();
          	for (int k = 0; k < corner_camera.at(j).size(); k++)
          	{
            	vs_msgs::cameraPoint corner_camera_msg;
            	corner_camera_msg.x = corner_camera.at(j).at(k).x;
            	corner_camera_msg.y = corner_camera.at(j).at(k).y;
            	corner_camera_msg.z = corner_camera.at(j).at(k).z;

            	corners_camera_msg.cameraPoints.push_back(corner_camera_msg);
          	}	
          }
          else
          	corners_camera_msg.num_cameraPoints = 0;

          

          /* Go to the next marker */
          break;
        }
      }

      /* Publish the message for the i-th marker */
      switch (i)
      {
        case 0:
          _aruco0_markerCorners_publisher.publish(corners_camera_msg);
          _aruco0_markerFeatures_publisher.publish(corner_features_msg);
          break;

        case 1:
          _aruco1_markerCorners_publisher.publish(corners_camera_msg);
          _aruco1_markerFeatures_publisher.publish(corner_features_msg);
          break;

        case 2:
          _aruco2_markerCorners_publisher.publish(corners_camera_msg);
          _aruco2_markerFeatures_publisher.publish(corner_features_msg);
          break;

        case 3:
          _aruco3_markerCorners_publisher.publish(corners_camera_msg);
          _aruco3_markerFeatures_publisher.publish(corner_features_msg);
          break;

        case 4:
          _aruco4_markerCorners_publisher.publish(corners_camera_msg);
          _aruco4_markerFeatures_publisher.publish(corner_features_msg);
          break;
      }
    }
  }
  else
  {
    corners_camera_msg.num_cameraPoints = 0;
    corner_features_msg.num_featurePoints = 0;

    _aruco0_markerCorners_publisher.publish(corners_camera_msg);
    _aruco0_markerFeatures_publisher.publish(corner_features_msg);
    _aruco1_markerCorners_publisher.publish(corners_camera_msg);
    _aruco1_markerFeatures_publisher.publish(corner_features_msg);
    _aruco2_markerCorners_publisher.publish(corners_camera_msg);
    _aruco2_markerFeatures_publisher.publish(corner_features_msg);
    _aruco3_markerCorners_publisher.publish(corners_camera_msg);
    _aruco3_markerFeatures_publisher.publish(corner_features_msg);
    _aruco4_markerCorners_publisher.publish(corners_camera_msg);
    _aruco4_markerFeatures_publisher.publish(corner_features_msg);
  }

  /* Threshold and contours */
  cv::Point3d dispenser_center_camera;
  geometry_msgs::Point dispenser_center_camera_msg;
  std::vector<std::vector<cv::Point>> contours_dispenser, contours_grasp, contours_deploy;
  detectContours(contours_dispenser);
  contours_grasp = contours_dispenser;
  contours_deploy = contours_dispenser;
  
  /* Find dispenser center and publish results */
  if(selectContours(contours_dispenser, _dispenser_roi_xmin, _dispenser_roi_xmax, _dispenser_roi_ymin, _dispenser_roi_ymax, _dispenser_area_min, _dispenser_area_max))
  {
    if (find_dispenser_center(contours_dispenser, dispenser_center_camera)) {
      dispenser_center_camera_msg.x = dispenser_center_camera.x;
      dispenser_center_camera_msg.y = dispenser_center_camera.y;
      dispenser_center_camera_msg.z = dispenser_center_camera.z;
      _dispenserCenterCamera_publisher.publish(dispenser_center_camera_msg);
    }
  }

  /* Deployment point in image plane*/
  if (!std::isnan(_actual_deploymentPoint_cameraFrame.x)
        || !std::isnan(_actual_deploymentPoint_cameraFrame.y)
        || !std::isnan(_actual_deploymentPoint_cameraFrame.z))
  {
    deploymentPoint_mutex.lock();
    cv::Point3d local_actual_deploymentPoint_cameraFrame = _actual_deploymentPoint_cameraFrame;
    deploymentPoint_mutex.unlock();
    cv::Point _actual_deploymentPoint_imagePlane, _visible_deploymentPoint;
    realsensecamera_cameraFrame2imagePlane(_actual_deploymentPoint_imagePlane, local_actual_deploymentPoint_cameraFrame, _color_intrin);
    vs_msgs::featurePoint deploymentPoint_msg;
    if(find_deployment_point(_actual_deploymentPoint_imagePlane, _visible_deploymentPoint))
    {
      deploymentPoint_msg.x = _visible_deploymentPoint.x;
      deploymentPoint_msg.y = _visible_deploymentPoint.y;
    }
    else
    {
      deploymentPoint_msg.x = deploymentPoint_msg.y = -1;
    }
    _deploymentPoint_imagePlane_publisher.publish(deploymentPoint_msg);
  }

  // Fill data structures useful for validate the grasping and deployment operation
  bool result;
  // check for grasping data
  result = (selectContours(contours_grasp, _check_grasp_roi_xmin, _frame_width, _check_grasp_roi_ymin, _frame_height, _check_grasp_area_min, _check_grasp_area_max) && check_grasp(contours_grasp));
  // Except for the beginning, I'll always have numMeasurementForValidation measurement in the list
  if(checkGraspData.size() == numMeasurementForValidation)
    checkGraspData.pop_back();
  checkGraspData.push_front(result);
  // check for deployment data
  result = (selectContours(contours_deploy, _check_deploy_roi_xmin, _frame_width, _check_deploy_roi_ymin, _frame_height, _check_deploy_area_min, _check_deploy_area_max) && check_deploy(contours_deploy));
  // Except for the beginning, I'll always have numMeasurementForValidation measurement in the list
  if(checkDeploymentData.size() == numMeasurementForValidation)
    checkDeploymentData.pop_back();
  checkDeploymentData.push_front(result);

}

/* Node services */
bool dispenser_imageprocessing::imagePlane2cameraFrame(dispenser_imageproc::imagePlane2cameraFrame::Request &req, dispenser_imageproc::imagePlane2cameraFrame::Response &res)
{
  cv::Point3d P_camera;
  float p_depth = req.p_depth;
  cv::Point p_image(req.px_image, req.py_image);

  realsensecamera_imagePlane2cameraFrame(p_image, p_depth, P_camera, _color_intrin);

  res.P_camera.x = P_camera.x;
  res.P_camera.y = P_camera.y;
  res.P_camera.z = P_camera.z;

  return true;
}


bool dispenser_imageprocessing::cameraFrame2imagePlane(dispenser_imageproc::cameraFrame2imagePlane::Request &req, dispenser_imageproc::cameraFrame2imagePlane::Response &res)
{
  cv::Point p_image;
  cv::Point3d P_camera(req.P_camera.x, req.P_camera.y, req.P_camera.z);

  realsensecamera_cameraFrame2imagePlane(p_image, P_camera, _color_intrin);

  res.px_image = p_image.x;
  res.py_image = p_image.y;

  return true;
}


bool dispenser_imageprocessing::getCameraIntrinsicParam(dispenser_imageproc::getCameraIntrinsicParam::Request &req, dispenser_imageproc::getCameraIntrinsicParam::Response &res)
{
  switch (req.camera_type) {
    case req.COLOR:
      res.height = _color_intrin.height;
      res.width  = _color_intrin.width;
      res.fx     = _color_intrin.fx;
      res.fy     = _color_intrin.fy;
      res.ppx    = _color_intrin.ppx;
      res.ppy    = _color_intrin.ppy;
    break;

    case req.DEPTH:
      res.height = _depth_intrin.height;
      res.width  = _depth_intrin.width;
      res.fx     = _depth_intrin.fx;
      res.fy     = _depth_intrin.fy;
      res.ppx    = _depth_intrin.ppx;
      res.ppy    = _depth_intrin.ppy;
    break;
  }

  return true;
}



/* Internal private functions */
bool dispenser_imageprocessing::realsensecamera_init(void)
{
  /* Initialize Realsense camera */
  if( _rs_ctx.get_device_count() > 0 )
  {
    _rs_camera = _rs_ctx.get_device(0);
    _rs_camera->enable_stream(rs::stream::color, _frame_width, _frame_height, rs::format::bgr8, _framerate);
    _rs_camera->enable_stream(rs::stream::depth, _frame_width, _frame_height, rs::format::z16, _framerate);
    _rs_camera->set_option(rs::option::r200_lr_auto_exposure_enabled, 1);
    _rs_camera->start();

    _depth_intrin         = _rs_camera->get_stream_intrinsics(rs::stream::depth);
    _color_intrin         = _rs_camera->get_stream_intrinsics(rs::stream::color);
    _scaleFromDepth2Color = _rs_camera->get_depth_scale();

#ifdef DEBUG
    /* Initialize debug windows */
    cv::namedWindow(_window_debug_marker, 0);
    cv::namedWindow(_window_debug_dispenser, 0);
    cv::namedWindow(_window_debug_deployment, 0);
#endif

    return true;
  }

  return false;
}

void dispenser_imageprocessing::realsensecamera_acquire(acquisition_type acq_type)
{
  /* Wait a frame from the camera */
  _rs_camera->wait_for_frames();

  /* Acquire a frame from the camera */
  switch(acq_type)
  {
    case COLOR: {
      _BGR_image = cv::Mat(_color_intrin.height, _color_intrin.width, CV_8UC3, (uchar *)_rs_camera->get_frame_data(rs::stream::color));

#ifdef DEBUG
    /* Store RGB image ad working copy */
    _debug_image_marker    = _BGR_image.clone();
    _debug_image_dispenser = _BGR_image.clone();
    _debug_image_deploy    = _BGR_image.clone();
#endif
    } break;

    case DEPTH: {
      _depth_image = (const uint16_t *)_rs_camera->get_frame_data(rs::stream::depth_aligned_to_color);
    } break;

    case ALL: {
      _BGR_image = cv::Mat(_color_intrin.height, _color_intrin.width, CV_8UC3, (uchar *)_rs_camera->get_frame_data(rs::stream::color));
      _depth_image = (const uint16_t *)_rs_camera->get_frame_data(rs::stream::depth_aligned_to_color);

#ifdef DEBUG
    /* Store RGB image ad working copy */
    _debug_image_marker    = _BGR_image.clone();
    _debug_image_dispenser = _BGR_image.clone();
    _debug_image_deploy    = _BGR_image.clone();
#endif
    } break;

    default:
      ROS_ERROR("Wrong acquisition type in realsensecamera_acquire");
  }
}

void dispenser_imageprocessing::realsensecamera_imagePlane2cameraFrame(cv::Point p_image, float p_depth, cv::Point3d& P_camera, rs::intrinsics intrinsic_param)
{
  float x_c  = (p_image.x - intrinsic_param.ppx)/intrinsic_param.fx;
  float y_c  = (p_image.y - intrinsic_param.ppy)/intrinsic_param.fy;
  float r2   = x_c*x_c + y_c*y_c;
  float f    = 1 + intrinsic_param.coeffs[0]*r2 + intrinsic_param.coeffs[1]*r2*r2 + intrinsic_param.coeffs[4]*r2*r2*r2;
  float ux_c = x_c*f + 2*intrinsic_param.coeffs[2]*x_c*y_c + intrinsic_param.coeffs[3]*(r2 + 2*x_c*x_c);
  float uy_c = y_c*f + 2*intrinsic_param.coeffs[3]*x_c*y_c + intrinsic_param.coeffs[2]*(r2 + 2*y_c*y_c);

  P_camera.x = ux_c*p_depth;
  P_camera.y = uy_c*p_depth;
  P_camera.z = p_depth;
}

void dispenser_imageprocessing::realsensecamera_cameraFrame2imagePlane(cv::Point& p_image, cv::Point3d P_camera, rs::intrinsics intrinsic_param)
{
  float x_c = P_camera.x/P_camera.z;
  float y_c = P_camera.y/P_camera.z;

  p_image.x = round(x_c * intrinsic_param.fx + intrinsic_param.ppx);
  p_image.y = round(y_c * intrinsic_param.fy + intrinsic_param.ppy);
}

bool dispenser_imageprocessing::realsensecamera_findDepth(cv::Point p_image, float& p_depth)
{ 
  uint16_t depth_value = _depth_image[(p_image.y) * _depth_intrin.width +(p_image.x)];
  p_depth = depth_value * _scaleFromDepth2Color;

  /* Check depth is inside the range */
  if (p_depth < _camera_min_depth || p_depth > _camera_max_depth)
  {
    p_depth = 0.0;
    return false;
  }

  return true;
}

bool dispenser_imageprocessing::find_aruco_marker(std::vector<int>& marker_ids, std::vector<std::vector<cv::Point2f>>& corner_features, std::vector<std::vector<cv::Point3d>>& corner_camera)
{ 
	corner_camera.clear();
	corner_features.clear();
	marker_ids.clear();

	/* Markers are detected in the image: for any marker, "id" is stored in the "marker_ids" vector, "features" in the "corner_features" vector*/
	cv::aruco::detectMarkers(_BGR_image, _aruco_dictionary, corner_features, marker_ids, _aruco_detector);	
	std::vector<cv::Point3d> corners_marker_camera;
	/* Iterator for detected markers */
	for (int i=0; i<marker_ids.size(); i++)
	{
    	corners_marker_camera.clear();
    	/* Iterator for features of any marker*/
    	for(int j=0; j<corner_features.at(i).size(); j++)
    	{
      		float p_depth;
      		cv::Point3d P_camera;
      		cv::Point p_image(corner_features.at(i).at(j).x, corner_features.at(i).at(j).y);
        	/* Corner from image plane to camera frame*/
      		if (realsensecamera_findDepth(p_image, p_depth))
      		{ 
		        realsensecamera_imagePlane2cameraFrame(p_image, p_depth, P_camera, _color_intrin);
		        corners_marker_camera.push_back(P_camera);
	    	}
    	}

    corner_camera.push_back(corners_marker_camera);
	}

#ifdef DEBUG
	  cv::aruco::drawDetectedMarkers(_debug_image_marker, corner_features, marker_ids);
  	cv::imshow(_window_debug_marker, _debug_image_marker);
  	cv::waitKey(1);
#endif

  	/* Check if all the corners has been detected */
  	if (corner_features.size()>0)
    	return true;

  	return false;
}

void dispenser_imageprocessing::detectContours(std::vector<std::vector<cv::Point>>& contours)
{
  /* Color segmentation */
  cv::Mat rgb_inv = ~_BGR_image;

  cv::Mat threshold_image, hsv_inv;
  cv::cvtColor(rgb_inv, hsv_inv, CV_BGR2HSV);
  inRange(hsv_inv, cv::Scalar(_hsv_thsd_min.at(0), _hsv_thsd_min.at(1), _hsv_thsd_min.at(2)),
                   cv::Scalar(_hsv_thsd_max.at(0), _hsv_thsd_max.at(1), _hsv_thsd_max.at(2)), threshold_image);
  cv::medianBlur(threshold_image, threshold_image, 5);
  cv::dilate(threshold_image, threshold_image, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)) ); 
  cv::erode(threshold_image, threshold_image, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)) );

  std::vector<cv::Vec4i> hierarchy;
  cv::findContours(threshold_image, contours, hierarchy, cv::RETR_LIST, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));
}

bool dispenser_imageprocessing::selectContours(std::vector<std::vector<cv::Point>>& contours, int x_min, int x_max, int y_min, int y_max, float area_min, float area_max)
{ 
  for(int i=0; i<contours.size(); i++)
  { 
    if(cv::contourArea(contours.at(i)) < area_min || cv::contourArea(contours.at(i)) > area_max)
    {
      contours.erase(contours.begin() + i);
      i--;
    }
    else
    {
      for(int j=0; j<contours.at(i).size(); j++)
      { 
        if (contours.at(i).at(j).x < x_min || contours.at(i).at(j).x > x_max || 
            contours.at(i).at(j).y < y_min || contours.at(i).at(j).y > y_max)
        {
          contours.erase(contours.begin() + i);
          i--;
          break;
        }
      }  
    }
  }

  if(contours.size()>0)
    return true;
  else
    return false;
}

bool dispenser_imageprocessing::find_dispenser_center(std::vector<std::vector<cv::Point>>& contours, cv::Point3d& dispenser_center_camera)
{
  for(int i=0; i<contours.size(); i++)
  {
    std::vector<cv::Point> convex;
    cv::convexHull(contours.at(i), convex);
    if(cv::matchShapes(contours.at(i), convex, CV_CONTOURS_MATCH_I1, 0) > _match_shapes_threshold)
    {
      contours.erase(contours.begin() + i);
      i--;
    }
  }

  /* Compute the center of the wings */
  float dispenser_depth_camera = 0.0;
  cv::Point dispenser_center_image = cv::Point(0,0);
  if(contours.size()==2){

#ifdef DEBUG
    cv::drawContours(_debug_image_dispenser, contours, -1, cv::Scalar(255,255,0));
#endif

    for(int i=0; i<contours.size(); i++)
    {
      cv::Moments mu = cv::moments(contours.at(i), false );
      cv::Point wing_position = cv::Point2f(mu.m10/mu.m00 , mu.m01/mu.m00);
#ifdef DEBUG
      cv::circle(_debug_image_dispenser, wing_position, 4, cv::Scalar(255,0,0), -1, 8, 0 );
#endif

      float depth;
      if(realsensecamera_findDepth(wing_position, depth))
      {
        dispenser_center_image.x = (dispenser_center_image.x + wing_position.x)/(i+1);
        dispenser_center_image.y = (dispenser_center_image.y + wing_position.y)/(i+1);
        dispenser_depth_camera   = (dispenser_depth_camera + depth)/(i+1);
      }
      else
      {
        //ROS_INFO("Depth cannot be computed");
        return false;
      }
    }
  }
  else
  {
    //ROS_INFO("Wings not found");
    return false;
  }

#ifdef DEBUG
  realsensecamera_imagePlane2cameraFrame(dispenser_center_image, dispenser_depth_camera, dispenser_center_camera, _color_intrin);
  cv::circle(_debug_image_dispenser, dispenser_center_image, 4, cv::Scalar(255,0,0), -1, 8, 0 );
  cv::rectangle(_debug_image_dispenser, cv::Point(_dispenser_roi_xmin, _dispenser_roi_ymin), cv::Point(_dispenser_roi_xmax, _dispenser_roi_ymax), cv::Scalar(255, 0, 0)); 
  cv::imshow(_window_debug_dispenser, _debug_image_dispenser);
  cv::waitKey(1);
#endif

  return true;
}

bool dispenser_imageprocessing::find_deployment_point(cv::Point Pdepl, cv::Point& deployment_point_image_plane)
{ 
  if(Pdepl.x >=0 && Pdepl.x < _frame_width && Pdepl.y >=0 && Pdepl.y < _frame_height)
  { 
    cv::Mat gray, filtered_image, threshold_image, binary, mod_gray;
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    std::vector<cv::Point> points;

    /* The gray image is filtered, then the Otsu's algorithm is applied to calculate a threshold value from image histogram.
    Now, the vine can be isolated in the gray image.  */
    cv::cvtColor(_BGR_image, gray, CV_BGR2GRAY);
    cv::blur(gray, filtered_image, cv::Size(5,5));
    // cv::threshold(filtered_image, binary, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);     /* Uncomment this line in case of real vine */
    cv::threshold(filtered_image, binary, 0, 255, CV_THRESH_BINARY_INV | CV_THRESH_OTSU);   /* Uncomment this line in case of simulation in lab */
    filtered_image -= binary;

    /* The deployment point in the image plane is known; pixels with similar value (gray scale) are isolated */
    filtered_image.copyTo(mod_gray);
    cv::floodFill(mod_gray, Pdepl, 255, 0, _floodfill_low, _floodfill_high, _floodfill_neighbours);
    cv::threshold(mod_gray, mod_gray, 254, 255, CV_THRESH_BINARY_INV);
    filtered_image -= mod_gray;

    /* Sobel operator is used to compute an approximation of the gradient of the image intensity function, then the Canny's algorithm detects edges */
    cv::Mat dx, dy;
    cv::Sobel(filtered_image,dx,CV_16S,1,0);
    cv::Sobel(filtered_image,dy,CV_16S,0,1);
    double CannyAccThresh = cv::threshold(filtered_image, threshold_image, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
    Canny(dx, dy, filtered_image, CannyAccThresh*0.5, CannyAccThresh);
    cv::dilate(filtered_image, filtered_image, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3))); 
    cv::erode(filtered_image, filtered_image, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3)));

    cv::findContours(filtered_image, contours, hierarchy, cv::RETR_LIST, cv::CHAIN_APPROX_NONE, cv::Point(0,0));
    
    double medium_distance = 0;
    for(int distance=0; distance < _distance_max; distance++)
    {
      for(int i=0; i<contours.size(); i++)
      {
        for(int j=0; j<contours.at(i).size(); j++)
        {
          double d = cv::norm(contours.at(i).at(j) - Pdepl);
          if(d <= distance)
          { 
            points.push_back(contours.at(i).at(j));
            medium_distance += d;
            contours.at(i).erase(contours.at(i).begin() + j);
            j--;
          }
        }
      }
    }

    if(points.size()>0)
    {
      medium_distance = (medium_distance/points.size())*1.1;
      for(int i=0; i<points.size(); i++)
      {
        double d = cv::norm(points.at(i) - Pdepl);
        if(d > medium_distance)
        { 
          points.erase(points.begin() + i);
          i--;
        }
      }
      
      cv::Point2f cntr;
      float r;
      minEnclosingCircle(points, cntr, r);
      deployment_point_image_plane.x = round(cntr.x);
      deployment_point_image_plane.y = round(cntr.y);
#ifdef DEBUG  
      cv::circle(_debug_image_deploy, cntr, (int)r, cv::Scalar(0, 255, 0), 2, 8, 0 );
      cv::circle(_debug_image_deploy, cntr, 3, cv::Scalar(0,255,0));
#endif  
    }
    else
      return false;

#ifdef DEBUG
    cv::circle(_debug_image_deploy, Pdepl, 4, cv::Scalar(255,0,0),-1);
    cv::imshow(_window_debug_deployment, _debug_image_deploy);
    cvWaitKey(1);
#endif

    return true;
  }
  else
  {
    ROS_INFO("Not in the image plane");
    return false;
  }
}

bool dispenser_imageprocessing::check_grasp(std::vector<std::vector<cv::Point>>& contours)
{ 
  for(int i=0; i<contours.size(); i++)
  {
    cv::Moments mu = cv::moments(contours.at(i), false );
    cv::Point wing_position = cv::Point2f(mu.m10/mu.m00 , mu.m01/mu.m00);
    float depth;
    realsensecamera_findDepth(wing_position, depth);
    if(depth > 0)
    {
      contours.erase(contours.begin() + i);
      i--;
    }    
  }

  if(contours.size() > 0)
    return true;
  return false;
}

bool dispenser_imageprocessing::check_deploy(std::vector<std::vector<cv::Point>>& contours)
{
  for(int i=0; i<contours.size(); i++)
  { 
    cv::Moments mu = cv::moments(contours.at(i), false );
    cv::Point wing_position = cv::Point2f(mu.m10/mu.m00 , mu.m01/mu.m00);
    float depth;
    realsensecamera_findDepth(wing_position, depth);
    if(depth > _check_deploy_depth_max)
    {
      contours.erase(contours.begin() + i);
      i--;
    }
  }

  for(int i=0; i<contours.size(); i++)
  {
    std::vector<cv::Point> convex;
    cv::convexHull(contours.at(i), convex);
    if(cv::matchShapes(contours.at(i), convex, CV_CONTOURS_MATCH_I1, 0) > _match_shapes_threshold)
    {
      contours.erase(contours.begin() + i);
      i--;
    }
  }

  if(contours.size()>=2)
    return true;
  else
    return false;
}
