# README
---
If you need to use this repo, please clone also **common_pkgs** (https://bitbucket.org/airlab-polimi/common_pkgs) repo in the same workspace. It contains packages that are shared with other repositories of the project.

---

To compile the package, follow these steps:

 - in vs\_grasping/src/grasping\_controller.cpp set CAMERA\_AVAILABLE to 1 if your camera is connected and you also want to test the visual servo part; 0 otherwise
 - in vs\_grasping/src/grasping\_controller.cpp set REAL\_ARM TO 1 if you are using the real Kinova arm, 0 if you are using a simulated one.
 - clone https://bitbucket.org/airlab-polimi/coordinator\_stub if you have not done it yet
 - compile with catkin\_make
 
In order to test the action server, follow these steps:

 - clone https://bitbucket.org/airlab-polimi/coordinator\_stub if you have not done it yet, and compile it with catkin\_make
 - if you want to use a simulated arm: roslaunch j2n6s300\_moveit\_config j2n6s300\_virtual\_robot\_demo.launch
 - if you want to use a real Kinova arm: roslaunch vs\_grasping kinova\_prepare.launch
 - roslaunch coordinator\_stub stubbed\_scan\_real\_manipulation.launch
 - trigger the whole procedure by publishing any message on /nextStep topic
 
 If you are running with a simulated coordinator and you want to modify the deployment point, change it in coordinator\_stub configuration files (_goal\_parameters.yaml_).

 **NOTES:** 

  - in case of _std::bad\_alloc_ error, try shutting down the node that publishes on /nextStep, and run a new one.
  - Currently, vis_servoing_pose is probably incorrect 

---

**TODO**

 - correct action feedbacks
 - set correct position of the poses in yaml
 