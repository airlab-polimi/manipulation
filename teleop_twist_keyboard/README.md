# teleop_twist_keyboard
Generic Keyboard Teleop for ROS
#Launch
To run: `rosrun teleop_twist_keyboard teleop_twist_keyboard.py`

#Usage
```
Reading from the keyboard and Publishing to cartesian_velocity!
---------------------------
w -> +x
x -> -x
d -> +y
a -> -y
r -> +z
v -> -z

p -> + rotation around x
o -> - rotation around x
l -> + rotation around y
k -> - rotation around y
m -> + rotation around z
n -> - rotation around z

CTRL-C to quit
```

