#!/usr/bin/env python
import roslib; roslib.load_manifest('teleop_twist_keyboard')
import rospy

from kinova_msgs.msg import PoseVelocity

import sys, select, termios, tty

msg = """
Reading from the keyboard and Publishing to cartesian_velocity!
---------------------------
w -> +x
x -> -x
d -> +y
a -> -y
r -> +z
v -> -z

p -> + rotation around x
o -> - rotation around x
l -> + rotation around y
k -> - rotation around y
m -> + rotation around z
n -> - rotation around z

CTRL-C to quit
"""
linear = {
		'w':(1,0,0),
		'd':(0,1,0),
		'r':(0,0,1),
		'x':(-1,0,0),
		'a':(0,-1,0),
		'v':(0,0,-1),
			}

angular = {
		'p':(1,0,0),
		'l':(0,1,0),
		'm':(0,0,1),
		'o':(-1,0,0),
		'k':(0,-1,0),
		'n':(0,0,-1),
			}

def getKey():
	tty.setraw(sys.stdin.fileno())
	select.select([sys.stdin], [], [], 0)
	key = sys.stdin.read(1)
	termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
	return key

if __name__=="__main__":
    	settings = termios.tcgetattr(sys.stdin)
	
	pub = rospy.Publisher('/j2n6s300_driver/in/cartesian_velocity', PoseVelocity, queue_size = 1)
	rospy.init_node('teleop_keyboard')

	linear_speed = 0.25
	angular_speed = 0.5
	x = 0
	y = 0
	z = 0
	wx = 0
	wy = 0
	wz = 0

	try:
		print msg
		while(1):
			key = getKey()
			if key in linear.keys():
				x = linear[key][0]
				y = linear[key][1]
				z = linear[key][2]
				wx = 0; wy = 0; wz = 0
			elif key in angular.keys():
				wx = angular[key][0]
				wy = angular[key][1]
				wz = angular[key][2]
				x = 0; y = 0; z = 0
			else:
				x = 0
				y = 0
				z = 0
				wx = 0
				wy = 0
				wz = 0
				if (key == '\x03'):
					break

			twist = PoseVelocity()
			twist.twist_linear_x = x*linear_speed; twist.twist_linear_y = y*linear_speed; twist.twist_linear_z = z*linear_speed;
			twist.twist_angular_x = wx*angular_speed; twist.twist_angular_y = wy*angular_speed; twist.twist_angular_z = wz*angular_speed
			pub.publish(twist)

	except:
		print e

	finally:
		twist = PoseVelocity()
		twist.twist_linear_x = 0; twist.twist_linear_y = 0; twist.twist_linear_z = 0;
		twist.twist_angular_x = 0; twist.twist_angular_y = 0; twist.twist_angular_z = 0
		pub.publish(twist)

    		termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)


