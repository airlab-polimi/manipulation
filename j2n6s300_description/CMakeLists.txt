cmake_minimum_required(VERSION 2.8.3)
project(j2n6s300_description)

find_package(catkin REQUIRED)

catkin_package()

install(DIRECTORY meshes
   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
   FILES_MATCHING PATTERN "*.dae"
 )
#install(DIRECTORY config DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION})
