#ifndef GRASPING_CONTROLLER_H_
#define GRASPING_CONTROLLER_H_

#include "ros/ros.h"
#include "tf/transform_listener.h"
#include "vs_msgs/featurePointArray.h"
#include "vs_msgs/featurePoint.h"
#include "vs_msgs/cameraPointArray.h"
#include "vs_msgs/cameraPoint.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Point.h"
#include "kinova_msgs/JointAngles.h"
#include "kinova_msgs/JointVelocity.h"
#include "vs_grasping/computeDesiredFeatures.h"
#include "vs_grasping/executeVisualControl.h"
#include "dispenser_imageproc/validateDeployment.h"
#include "dispenser_imageproc/validateGrasping.h"
#include <eigen3/Eigen/Dense>

#include "std_msgs/Bool.h"
#include "std_msgs/Empty.h"
// advertised services
#include "grape_msgs/ArmToNavigationPose.h"
#include "grape_msgs/ArmEmergencyStop.h"
#include "vs_grasping/GetJointsSafeForWires.h"
// moveit
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>

// action
#include <grape_msgs/ArmManipulationAction.h>
#include <actionlib/server/simple_action_server.h>
// threads_vcam_camera
#include <mutex>


#define RUN_PERIOD_DEFAULT 0.01
/* Used only if the actual value of the period is not retrieved from the ROS parameter server */
#define NAME_OF_THIS_NODE "grasping_controller"

typedef actionlib::SimpleActionServer<grape_msgs::ArmManipulationAction> ArmManipulationServer;


//-----------------------------------------------------------------------

// Enum for the possible available modes of deployment: 
// 1) with moveit
// 2) with visual servo on the nail
// 3) with visual servo on the plant

enum class DeploymentMode{ 
    MoveIt,
    VisualServoNail,
    VisualServoPlant    
};

//-----------------------------------------------------------------------


class grasping_controller
{
  private:
        ros::NodeHandle Handle;

    // Action server for the dispenser deployement
        ArmManipulationServer armManipulationServer;
        // messages for sending feedback and result
        grape_msgs::ArmManipulationFeedback armManipulationFeedback;
        grape_msgs::ArmManipulationResult armManipulationResult;
        // new goal function
        void newArmManipulationReceived(const grape_msgs::ArmManipulationGoalConstPtr &goal);        
        // enum for deployment mode
        DeploymentMode deploymentMode;


    // Published and subscribed topics
    // --------------------
        ros::Subscriber _toolPose_subscriber, _jointPositions_subscriber;
        ros::Subscriber _arucoMarkerFeatures_0_subscriber, _arucoMarkerFeatures_1_subscriber, _arucoMarkerFeatures_2_subscriber, _arucoMarkerFeatures_3_subscriber, _arucoMarkerFeatures_4_subscriber;
        // ros::Subscriber _arucoMarkerCorners_0_subscriber;
        ros::Subscriber _dispenserCenterCamera_subscriber;
        ros::Publisher  _cartesianVelocity_publisher, _jointVelocity_publisher;

        ros::Publisher _deploymentPoint_publisher;
        ros::Subscriber _deploymentPoint_subscriber;

        /* ROS topic callbacks */
        void toolPose_MessageCallback(const geometry_msgs::PoseStamped::ConstPtr& pose);
        void jointPositions_MessageCallback(const kinova_msgs::JointAngles::ConstPtr& jointPositions);
        void aruco0_markerFeatures_MessageCallback(const vs_msgs::featurePointArray::ConstPtr& marker_features);
        void aruco1_markerFeatures_MessageCallback(const vs_msgs::featurePointArray::ConstPtr& marker_features);
        void aruco2_markerFeatures_MessageCallback(const vs_msgs::featurePointArray::ConstPtr& marker_features);
        void aruco3_markerFeatures_MessageCallback(const vs_msgs::featurePointArray::ConstPtr& marker_features);
        void aruco4_markerFeatures_MessageCallback(const vs_msgs::featurePointArray::ConstPtr& marker_features);
        // void aruco0_markerCorners_MessageCallback(const vs_msgs::cameraPointArray::ConstPtr& marker_camera);
        void dispenserCenterCamera_MessageCallback(const geometry_msgs::Point::ConstPtr& dispencerCenter_camera);
	    void deploymentPoint_MessageCallback(const vs_msgs::featurePoint& deploymentPoint);

    // ROS services
    // --------------------
        ros::ServiceClient _cameraFrame2imagePlane_cli;
        ros::ServiceClient _getCameraIntrinsicParam_cli;
        ros::ServiceClient _take_snaps_cli;
        ros::ServiceClient _validate_deployment;
        ros::ServiceClient _validate_grasping;

        ros::ServiceServer _armToNavigationPose;
        bool armToNavigationPose(grape_msgs::ArmToNavigationPose::Request &req, grape_msgs::ArmToNavigationPose::Response &res);
        ros::ServiceServer _armEmergencyStop;
        bool armEmergencyStop(grape_msgs::ArmEmergencyStop::Request &req, grape_msgs::ArmEmergencyStop::Response &res);
        ros::ServiceServer _getJointSafeForWires;
        bool getJointsSafeForWires(vs_grasping::GetJointsSafeForWires::Request &req, vs_grasping::GetJointsSafeForWires::Response &res);
    // Sensor parameters
    // --------------------
        /* Camera pose */
        Eigen::Vector3f _PcEef;
        Eigen::Matrix3f _RcEef;
        /* Camera intrisic */
        double _colCamera_fx, _colCamera_fy;
        double _colCamera_ppx, _colCamera_ppy;
        /* End-effector pose */
        Eigen::Matrix3f _ReefO;
        Eigen::Vector3f _PeefO;
        Eigen::Vector3f _PnailsEef;
        // this is the same information as before, but inside a geometry_msg::Pose format
        geometry_msgs::Pose toolCurrentPose;

    
    // Arm control 
    // --------------------

        // wires handling
        std::vector<double> jointPositions_untangled;
        void saveUntangledJointPositions();
        void untangle_joint0();

        void initManipulator();
        void translatePlan2currentJacoPosition(moveit::planning_interface::MoveGroupInterface::Plan& plan);
        void translateTargetPoint(geometry_msgs::Point& pose);
        // Move the fingers in the specified point in joint space
        bool moveFingers(std::vector<double> targetFingersPose);
        // Move the arm in the specified point in joint space 
        bool moveArm(std::vector<double> targetArmPose);
        // Move the end effector in the specified point in cartesian space,
        // oriented as required in deployment phase
        bool setEndEffectorPose(geometry_msgs::Point targetPoint);

        bool moveEndEffectorWithConstantOrientation(geometry_msgs::Point finalPoint);
        bool moveEndEffectorWithConstantOrientation(geometry_msgs::Vector3 offset);
        bool moveAlongZeef(float offset);

        geometry_msgs::Point getPointAlongDirection(geometry_msgs::Point initialPoint, geometry_msgs::Vector3 direction, float distance);
        void rotation_eef();

        // moveit
        moveit::planning_interface::MoveGroupInterface::Plan plan;
        moveit::planning_interface::MoveGroupInterface group;
        robot_model_loader::RobotModelLoader jaco2_model_loader;
        robot_model::RobotModelPtr jaco2_kinematic_model;
        robot_state::RobotStatePtr jaco2_kinematic_state;
        robot_state::JointModelGroup* jaco2_joint_model_group;

        // This is a point close to the deployment point, that also belongs to the vector normal wrt the deplyment point
        geometry_msgs::Point safePosition;
        geometry_msgs::Point checkDeployPosition;

        // pre-selected positions for the arm
        std::vector<double> navigation_pose;
        std::vector<double> test_pose;
        std::vector<std::vector<double> > prepicking_poses;
        std::vector<std::vector<double> > vis_servoing_poses;
        std::vector<double> nail_pose;
        geometry_msgs::Vector3 postpicking_offset;
        std::vector<double> plantDeploymentOrientation;
        // pre-selected positions for the fingers
        std::vector<double> finger_prepick_position;
        std::vector<double> finger_pick_position;
        std::vector<double> finger_open;
        std::vector<double> finger_close;
        // deployment procedure parameters
        geometry_msgs::Point lowered_targetPoint, goal_point;
        double plantSafetyDistance, checkDeploymentDistance;

        // validation of operations
        bool checkDeployment();
        bool checkGrasping();

        //rotation of end effector for untangle the wires
        void publishEndEffectorSpeed();
        bool rotateEndEffector;
        double targetJointRotation;
        double initialJointRotation;
        double distanceFromTargetTolerance;
        std::vector<double> joint_values;
        // dummy function for testing the untanglement procedure
        void prova(const std_msgs::Empty::ConstPtr& dummy);
        ros::Subscriber testTopic;
        void rotateEndEffectorBack();
        kinova_msgs::JointVelocity end_effector_velocity_cmd;





    // Visual servo 
    // --------------------
        enum vscontroller_type {GRASP, DEPLOY};
        vs_msgs::featurePointArray executeVisualControl(const vscontroller_type controller_type, float desiredDepth, vs_msgs::featurePointArray desiredFeatures, int heightIndex);
	    vs_msgs::featurePointArray executeVisualControlDeplPart(float desiredDepth, vs_msgs::featurePointArray desiredFeatures);
        
        // Controller periodic task
        void visualServoing_PeriodicTask(const vscontroller_type controller_type, const std::vector<vs_msgs::featurePoint>& desired_features, double Z_camera, Eigen::VectorXf& feature_error, int *id_marker, int heightIndex);
        void visualServoingPartitioned_PeriodicTask(const geometry_msgs::Point P_world, const std::vector<vs_msgs::featurePoint>& desired_features, double Z_camera, Eigen::VectorXf& feature_error, float& z_distance);
        // Compute marker fetures 
        bool computeDesiredFeatures(vs_msgs::featurePointArray& desiredFeatures, float *desiredDepth, int& heightIndex);
        /* Visual servoing controller variables */
        std::vector<vs_msgs::featurePoint> _actual_arucoFeatures_0, _actual_arucoFeatures_1, _actual_arucoFeatures_2, _actual_arucoFeatures_3, _actual_arucoFeatures_4;
        // std::vector<vs_msgs::cameraPoint> _actual_arucoCamera_0;

        vs_msgs::featurePoint _actual_deployFeatures;

        geometry_msgs::Point _actual_dispencerCenter_camera;
        Eigen::VectorXd _jointVelocity;
        Eigen::MatrixXd _jacobian;
        Eigen::MatrixXf _Ls;
        Eigen::VectorXf _vcam_camera;

        std::vector<double> _heightVector;
        std::vector<vs_msgs::featurePointArray> _defaultFeatures;
        std::vector<vs_msgs::featurePoint> _deployingDesiredFeatures;
        vs_msgs::featurePoint _deployingDispenserCenter;

        std::vector<int> _aruco0_to_aruco1_featureTranslation, _aruco0_to_aruco2_featureTranslation, _aruco0_to_aruco3_featureTranslation;
        double k0, kinf, kslope, _max_visual_error_norm, _min_distance_VS;
        float Ks_z, Ks_xy;

    // General behaviour parameters 
    // --------------------
        std::vector<double> _grasping_frame_offset;
        float post_deploy_nail_offset, deploy_VS_last_step;
        geometry_msgs::Vector3 pre_deploy_VS_offset;

    // Bools to regulate shared variables access
    // --------------------
        // std::mutex actual_arucoCamera_0_mutex;
        std::mutex actual_arucoFeatures_0_mutex, actual_arucoFeatures_1_mutex, actual_arucoFeatures_2_mutex, actual_arucoFeatures_3_mutex, actual_arucoFeatures_4_mutex;
        std::mutex actual_dispencerCenter_camera_mutex;
        std::mutex toolPose_mutex, jointPosition_mutex;
        std::mutex actual_deployFeatures_mutex;

    // Helper functions
    // --------------------
        /* Frame conversion functions */
        void camera2world(geometry_msgs::Point P_camera, geometry_msgs::Point& P_world);
	    void world2camera(geometry_msgs::Point P_world, geometry_msgs::Point& P_camera);
        void initParameters();
        void debug();
        void selectDeploymentMode(const grape_msgs::ArmManipulationGoalConstPtr &goal);
        void deploymentProcedure();

  public:

    double RunPeriod;

    grasping_controller();
    void Prepare(void);
    void RunPeriodically(float Period);
    void Shutdown(void);
};


#endif /* GRASPING_CONTROLLER_H_ */
