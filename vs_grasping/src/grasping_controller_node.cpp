#include "vs_grasping/grasping_controller.h"


int main(int argc, char **argv)
{
    ros::init(argc, argv, NAME_OF_THIS_NODE);
  
  	sleep(4);
  	ros::AsyncSpinner spinner(1);
  	spinner.start();
    grasping_controller grasping_controller_node;
    grasping_controller_node.Prepare();
  	spinner.stop();
    ROS_INFO("Ok, node running.");
    grasping_controller_node.RunPeriodically(grasping_controller_node.RunPeriod);
    grasping_controller_node.Shutdown();
  
    return (0);
}
