#include "vs_grasping/grasping_controller.h"
#include "vs_msgs/featurePointArray.h"
#include "kinova_msgs/PoseVelocity.h"
#include "kinova_msgs/JointVelocity.h"
#include "dispenser_imageproc/cameraFrame2imagePlane.h"
#include "dispenser_imageproc/getCameraIntrinsicParam.h"
// move arm and fingers
#include <kinova_msgs/SetFingersPositionAction.h>
// actionlib
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>

#define CAMERA_AVAILABLE 1
#define REAL_ARM 1

#define NODE_RATE 100


//------------------------------------------------------------------------
// Public methods
//------------------------------------------------------------------------

grasping_controller::grasping_controller(): 
                        group("arm"),
                        jaco2_model_loader("robot_description"),
                        Handle(),
                        armManipulationServer(Handle,
                                              "arm_manipulation",
                                              boost::bind(&grasping_controller::newArmManipulationReceived,this, _1)),
                        prepicking_poses(2),
                        vis_servoing_poses(2)
{
    // Initialize MoveIt! for joint control
    jaco2_kinematic_model = jaco2_model_loader.getModel();
    jaco2_kinematic_state = robot_state::RobotStatePtr(new robot_state::RobotState(jaco2_kinematic_model));
    jaco2_joint_model_group = jaco2_kinematic_model->getJointModelGroup("arm");
    rotateEndEffector = false;
}


void grasping_controller::Prepare() {
    initParameters();
    initManipulator();

    /* ROS topics */
    _toolPose_subscriber              = Handle.subscribe("j2n6s300_driver/out/tool_pose", 1, &grasping_controller::toolPose_MessageCallback, this);
    _jointPositions_subscriber        = Handle.subscribe("j2n6s300_driver/out/joint_angles", 1, &grasping_controller::jointPositions_MessageCallback, this);
    _arucoMarkerFeatures_0_subscriber = Handle.subscribe("/dispenser_imgproc/aruco0_featurePoints", 1, &grasping_controller::aruco0_markerFeatures_MessageCallback, this);
    _arucoMarkerFeatures_1_subscriber = Handle.subscribe("/dispenser_imgproc/aruco1_featurePoints", 1, &grasping_controller::aruco1_markerFeatures_MessageCallback, this);
    _arucoMarkerFeatures_2_subscriber = Handle.subscribe("/dispenser_imgproc/aruco2_featurePoints", 1, &grasping_controller::aruco2_markerFeatures_MessageCallback, this);
    _arucoMarkerFeatures_3_subscriber = Handle.subscribe("/dispenser_imgproc/aruco3_featurePoints", 1, &grasping_controller::aruco3_markerFeatures_MessageCallback, this);
    _arucoMarkerFeatures_4_subscriber = Handle.subscribe("/dispenser_imgproc/aruco4_featurePoints", 1, &grasping_controller::aruco4_markerFeatures_MessageCallback, this);
    // _arucoMarkerCorners_0_subscriber  = Handle.subscribe("/dispenser_imgproc/aruco0_cornersCamera", 1, &grasping_controller::aruco0_markerCorners_MessageCallback, this);
    _dispenserCenterCamera_subscriber = Handle.subscribe("/dispenser_imgproc/dispenser_centerCamera", 1, &grasping_controller::dispenserCenterCamera_MessageCallback, this);
    
    _cartesianVelocity_publisher      = Handle.advertise<kinova_msgs::PoseVelocity>("/j2n6s300_driver/in/cartesian_velocity", 1);
    _jointVelocity_publisher          = Handle.advertise<kinova_msgs::JointVelocity>("/j2n6s300_driver/in/joint_velocity", 1);
    
    _deploymentPoint_publisher        = Handle.advertise<geometry_msgs::Point>("/grasping_controller/deployment_point_camera_frame", 1);
    _deploymentPoint_subscriber       = Handle.subscribe("/dispenser_imgproc/deploymentPoint_imagePlane", 1, &grasping_controller::deploymentPoint_MessageCallback, this);

    /* Subscribed services */
    _cameraFrame2imagePlane_cli  = Handle.serviceClient<dispenser_imageproc::cameraFrame2imagePlane>("cameraFrame2imagePlane");
    _getCameraIntrinsicParam_cli = Handle.serviceClient<dispenser_imageproc::getCameraIntrinsicParam>("getCameraIntrinsicParam");
    _validate_deployment         = Handle.serviceClient<dispenser_imageproc::validateDeployment>("validateDeployment");
    _validate_grasping           = Handle.serviceClient<dispenser_imageproc::validateGrasping>("validateGrasping");
    /* Advertised services */
    _armToNavigationPose  = Handle.advertiseService("arm_to_nav_pose", &grasping_controller::armToNavigationPose, this);
    _armEmergencyStop     = Handle.advertiseService("arm_emergency_stop", &grasping_controller::armEmergencyStop, this);
    _getJointSafeForWires = Handle.advertiseService("get_joints_safe_for_wires", &grasping_controller::getJointsSafeForWires, this);


#if CAMERA_AVAILABLE
    dispenser_imageproc::getCameraIntrinsicParam srv;
    srv.request.camera_type = srv.request.COLOR;
    while (!_getCameraIntrinsicParam_cli.call(srv)) {
        ROS_ERROR("Not able to get camera intrinsic parameters, camera is probably disconnected or has not been recognized yet");
        ros::Duration(0.5).sleep();
    }
    _colCamera_fx  = srv.response.fx;
    _colCamera_fy  = srv.response.fy;
    _colCamera_ppx = srv.response.ppx;
    _colCamera_ppy = srv.response.ppy;
#endif
    // testing untangling procedure
    ROS_INFO("Node %s ready to run.", ros::this_node::getName().c_str());
    geometry_msgs::Pose myPose = group.getCurrentPose().pose;
    ROS_INFO("pose attuale: %f %f %f, %f %f %f %f", myPose.position.x, myPose.position.y, myPose.position.z,
                                                    myPose.orientation.x, myPose.orientation.y, myPose.orientation.z, myPose.orientation.w);
    
    testTopic = Handle.subscribe("test_topic", 1, &grasping_controller::prova, this); 
    saveUntangledJointPositions();
    moveFingers(finger_close);
    moveArm(navigation_pose);
    untangle_joint0();
}



void grasping_controller::RunPeriodically(float Period)
{
    ros::Rate LoopRate(1.0/Period);

    ROS_INFO("Node %s running periodically (T=%.2fs, f=%.2fHz).", ros::this_node::getName().c_str(), Period, 1.0/Period);

    while (ros::ok()){
        ros::spinOnce();
        LoopRate.sleep();
    }
}


void grasping_controller::Shutdown(void)
{
    ROS_INFO("Node %s shutting down.", ros::this_node::getName().c_str());
}

//------------------------------------------------------------------------
// ArmManipulation action functions: handles the Finite State Machine described in README.md
//-----------------------------------------------------------------------

void grasping_controller::newArmManipulationReceived(const grape_msgs::ArmManipulationGoalConstPtr &goal) {
    ROS_INFO("\n\n\n\n\n New goal received. Size of the array in the goal: %d \n\n\n ", int(goal->deployment_poses.size()));

    geometry_msgs::Point pointInBaseLink = goal->deployment_poses[0].deployment_point;
    ROS_INFO("Normal: %f %f %f",goal->deployment_poses[0].normal_vector.x,goal->deployment_poses[0].normal_vector.y,goal->deployment_poses[0].normal_vector.z);
    ROS_INFO("Point: %f %f %f",goal->deployment_poses[0].deployment_point.x,goal->deployment_poses[0].deployment_point.y,goal->deployment_poses[0].deployment_point.z);

    // in order to let the dispenser be deployed more safely, the goal position *for the fingers* is slightly translated toward the ground
    // In this way, the fingers are less likely to hit the branch
    lowered_targetPoint.x = pointInBaseLink.x;
    lowered_targetPoint.y = pointInBaseLink.y;
    lowered_targetPoint.z = pointInBaseLink.z;
    ROS_INFO("Deployment point: %f, %f, %f\n", lowered_targetPoint.x, lowered_targetPoint.y, lowered_targetPoint.z);
    safePosition = getPointAlongDirection(lowered_targetPoint, goal->deployment_poses[0].normal_vector, plantSafetyDistance);
    checkDeployPosition = getPointAlongDirection(lowered_targetPoint, goal->deployment_poses[0].normal_vector, checkDeploymentDistance);
    ROS_INFO("Safe position: %f, %f, %f", safePosition.x, safePosition.y, safePosition.z);

    // 1) Move robot to the position where the camera computes the desired features
    moveArm(prepicking_poses.at(0));
#if REAL_ARM
    moveFingers(finger_prepick_position);
#else
    ROS_INFO("Fingers in prepick position");
#endif

    armManipulationFeedback.status.status_string = armManipulationFeedback.status.pre_pick_pose_string;
    armManipulationFeedback.status.status_code = armManipulationFeedback.status.pre_pick_pose;
    armManipulationServer.publishFeedback(armManipulationFeedback);

    // 2) Look for the marker features
#if CAMERA_AVAILABLE
    vs_msgs::featurePointArray desiredFeatures;
    float desiredDepth;
    int heightIndex;
 
    geometry_msgs::PointConstPtr p = ros::topic::waitForMessage<geometry_msgs::Point>("/dispenser_imgproc/dispenser_centerCamera", ros::Duration(5.0));
    if (!p || !computeDesiredFeatures(desiredFeatures, &desiredDepth, heightIndex)) {
        armManipulationFeedback.status.status_string = armManipulationFeedback.status.no_dispenser_found_string;
        armManipulationFeedback.status.status_code = armManipulationFeedback.status.no_dispenser_found;
        armManipulationServer.publishFeedback(armManipulationFeedback);
        moveArm(navigation_pose);
        untangle_joint0();
        armManipulationServer.setAborted(armManipulationResult);
        return;
    }
    ROS_INFO("Desired features found");
#endif
    armManipulationFeedback.status.status_string = armManipulationFeedback.status.marker_computed_features_string;
    armManipulationFeedback.status.status_code = armManipulationFeedback.status.marker_computed_features;
    armManipulationServer.publishFeedback(armManipulationFeedback);

    // 3) Move robot to the visual servoing starting position
    moveArm(vis_servoing_poses.at(0));
    armManipulationFeedback.status.status_string = armManipulationFeedback.status.picking_up_dispenser_string;
    armManipulationFeedback.status.status_code = armManipulationFeedback.status.picking_up_dispenser;
    armManipulationServer.publishFeedback(armManipulationFeedback);

    // 4) Activate visual servoing, and reach the grasping position 
    #if CAMERA_AVAILABLE
        vs_msgs::featurePointArray residualFeatureErrors;
        residualFeatureErrors = executeVisualControl(GRASP, desiredDepth, desiredFeatures, heightIndex);
    #endif
    armManipulationFeedback.status.status_string = armManipulationFeedback.status.grasping_dispenser_string;
    armManipulationFeedback.status.status_code = armManipulationFeedback.status.grasping_dispenser;
    armManipulationServer.publishFeedback(armManipulationFeedback);

    // 5) Grasp the dispenser
#if REAL_ARM
    moveFingers(finger_open);
# else
    ROS_INFO("Open fingers");
#endif
    ROS_INFO("Esco dal feeder");
    // Move to higher position in vertical direction
    moveEndEffectorWithConstantOrientation(postpicking_offset);
    ROS_INFO("uscito dal feeder");    
    armManipulationFeedback.status.status_string = armManipulationFeedback.status.dispenser_grasped_string;
    armManipulationFeedback.status.status_code = armManipulationFeedback.status.dispenser_grasped;
    armManipulationServer.publishFeedback(armManipulationFeedback);

    checkGrasping();
    // if(!checkGrasping()){
    //     armManipulationFeedback.status.status_string = armManipulationFeedback.status.grasping_failed_string;
    //     armManipulationFeedback.status.status_code = armManipulationFeedback.status.grasping_failed;
    //     armManipulationServer.publishFeedback(armManipulationFeedback);
    //     armManipulationServer.setAborted(armManipulationResult);
    //     moveArm(navigation_pose);
    //     rotateEndEffectorBack();
    //     return;
    // }

    moveFingers(finger_open);
    selectDeploymentMode(goal);
    if(deploymentMode != DeploymentMode::VisualServoNail) {
        // Move to the safe position, frontal to the deployment point
        ROS_INFO("vado in safe position");
        if(setEndEffectorPose(safePosition))
        {
            ROS_INFO("Sono in safe position: %f, %f, %f", safePosition.x, safePosition.y, safePosition.z);
            rotation_eef();
            ros::Duration(2).sleep();
            goal_point = lowered_targetPoint;
            // The desired position of the end effector is computed considering actual orientation and offset between eef frame and nails
            translateTargetPoint(lowered_targetPoint);
            ROS_INFO("Desired eef pose: %f, %f, %f", lowered_targetPoint.x, lowered_targetPoint.y, lowered_targetPoint.z); 
        }
        else {
            ROS_INFO("Can't plan to safe position");
            moveArm(navigation_pose);
            untangle_joint0();
            armManipulationServer.setAborted(armManipulationResult);
            return;
        }
    ROS_INFO("andato in safe position");
        armManipulationFeedback.status.status_string = armManipulationFeedback.status.frontal_pose_reached_string;
        armManipulationFeedback.status.status_code = armManipulationFeedback.status.frontal_pose_reached;
        armManipulationServer.publishFeedback(armManipulationFeedback);
    }
    // Deployment phase: try to deploy, move back the arm to have a view on the scene, and use the camera to check 
    // if deployment has been successful or not
    deploymentProcedure();

    if(!checkDeployment()){
        armManipulationFeedback.status.status_string = armManipulationFeedback.status.deployment_failed_string;
        armManipulationFeedback.status.status_code = armManipulationFeedback.status.deployment_failed;
        armManipulationServer.publishFeedback(armManipulationFeedback);
        moveArm(navigation_pose);
        untangle_joint0();
        armManipulationServer.setAborted(armManipulationResult);
        return;
    }
    armManipulationFeedback.status.status_string = armManipulationFeedback.status.dispenser_placed_string;
    armManipulationFeedback.status.status_code = armManipulationFeedback.status.dispenser_placed;
    armManipulationServer.publishFeedback(armManipulationFeedback);

    // 11) Move back to park position
    moveArm(navigation_pose);
    untangle_joint0();
    armManipulationFeedback.status.status_string = armManipulationFeedback.status.arm_parked_string;
    armManipulationFeedback.status.status_code = armManipulationFeedback.status.arm_parked;
    armManipulationServer.publishFeedback(armManipulationFeedback);

    // End of the sequence!
    armManipulationServer.setSucceeded(armManipulationResult);
}


//---------------------------------------------------------------
//---------------------------------------------------------------
//---------------------------------------------------------------

void grasping_controller::prova(const std_msgs::Empty::ConstPtr& dummy){
    return;
}



void grasping_controller::untangle_joint0(){
    double targetPosition;
    double referencePosition;
    double currentPosition;
    currentPosition = joint_values.at(0);
    referencePosition = jointPositions_untangled.at(0);

    targetPosition = currentPosition + 2*M_PI*std::min(std::ceil((referencePosition - currentPosition - M_PI)/(2*M_PI)),  std::floor((referencePosition - currentPosition + M_PI)/(2*M_PI)));
    // targetPosition = referencePosition + std::fmod(currentPosition,2*M_PI) - std::fmod(referencePosition,2*M_PI);


    if(std::fabs(targetPosition - currentPosition) < 2*M_PI){
        ROS_INFO("No need to untangle");
        return;
    }
    double sign = (targetPosition - currentPosition > 0) ? 1 : -1;
    
    kinova_msgs::JointVelocity joint0_velocity;
    joint0_velocity.joint1 = sign*30;
    joint0_velocity.joint2 = 0;
    joint0_velocity.joint3 = 0;
    joint0_velocity.joint4 = 0;
    joint0_velocity.joint5 = 0;
    joint0_velocity.joint6 = 0;

    ros::Rate r(NODE_RATE);
    while (std::fabs(targetPosition - currentPosition) > distanceFromTargetTolerance)
    {
        ros::spinOnce();
        currentPosition = joint_values.at(0);
        _jointVelocity_publisher.publish(joint0_velocity);
        r.sleep();
    }

}

void grasping_controller::saveUntangledJointPositions(){
    while(joint_values.size() == 0)
        ros::Duration(0.1).sleep();

    jointPosition_mutex.lock();
    ////////////////////////////////////////
    jointPositions_untangled = {joint_values.at(0),
                                joint_values.at(1),
                                joint_values.at(2),
                                joint_values.at(3),
                                joint_values.at(4),
                                joint_values.at(5)};
    ////////////////////////////////////////
    ROS_INFO("Retrieved joint values safe for the wires");
    jointPosition_mutex.unlock();
}


void grasping_controller::selectDeploymentMode(const grape_msgs::ArmManipulationGoalConstPtr &goal){
    if(goal->deployMode == goal->nailMode){
        deploymentMode = DeploymentMode::VisualServoNail;
        ROS_INFO("Nail mode");
    }
    else if (goal->deployMode == goal->visServoMode) {
        deploymentMode = DeploymentMode::VisualServoPlant;
        ROS_INFO("Vis servo on plant");
    }
    // default choice
    else {
        deploymentMode = DeploymentMode::MoveIt;
        ROS_INFO("MoveIt mode");
    }
}

bool grasping_controller::checkDeployment(){
    // wait a couple of secs before checking, waiting for the analyzed frames to get stable
    ros::Duration(3).sleep();
    dispenser_imageproc::validateDeployment srv;
    if(_validate_deployment.call(srv)){
        return srv.response.is_dispenser_deployed;
    } else {
        ROS_ERROR("Can't contact server for validation of deployment");
        return 1;
    }
}

bool grasping_controller::checkGrasping(){
    // wait a couple of secs before checking, waiting for the analyzed frames to get stable
    ros::Duration(3).sleep();
    dispenser_imageproc::validateGrasping srv;
    if(_validate_grasping.call(srv)){
        return srv.response.is_dispenser_grasped;
    } else {
        ROS_ERROR("Can't contact server for validation of deployment");
        return 1;
    }
}

void grasping_controller::deploymentProcedure(){

    switch(deploymentMode){
    case DeploymentMode::VisualServoNail : {
            float desiredDepth;
            vs_msgs::featurePointArray desiredFeatures;
            vs_msgs::featurePointArray residualFeatureErrors;

            // Place the arm frontal to the nail
            ROS_INFO("vado in nail pose");
            moveArm(nail_pose);
            ROS_INFO("sono in nail pose");

            // 9b) Visual servoing (marker with ID 4)
            desiredDepth = std::fabs(_PnailsEef(2)) + std::fabs(_PcEef(2));
            desiredFeatures.num_featurePoints = 4;
            desiredFeatures.featurePoints = _deployingDesiredFeatures;
            // in DEPLOY mode, heightIndex (last param) is not used; pass a random value
            residualFeatureErrors = executeVisualControl(DEPLOY, desiredDepth, desiredFeatures, -1);
            moveFingers(finger_close);

            moveAlongZeef(post_deploy_nail_offset);
        }
            break;
    case DeploymentMode::VisualServoPlant : {
        #if CAMERA_AVAILABLE
            // Move the arm towards the ground to be sure that the deployment point is in the image plane
            moveEndEffectorWithConstantOrientation(pre_deploy_VS_offset);
            float desiredDepth = std::fabs(_PnailsEef(2)) + std::fabs(_PcEef(2));
            vs_msgs::featurePointArray desiredFeatures;
            desiredFeatures.num_featurePoints = 1;
            desiredFeatures.featurePoints = {_deployingDispenserCenter};
            vs_msgs::featurePointArray residualFeatureErrors;
            residualFeatureErrors = executeVisualControlDeplPart(desiredDepth, desiredFeatures);

            moveAlongZeef(deploy_VS_last_step);
            moveFingers(finger_close);
            moveAlongZeef(-deploy_VS_last_step);

            moveEndEffectorWithConstantOrientation(checkDeployPosition);
        #endif
        }
            break;

    case DeploymentMode::MoveIt :
    default:
        {
            // Move to deployment pose, and release the dispenser
            ROS_INFO("Sto andando in deployment point");
            moveEndEffectorWithConstantOrientation(lowered_targetPoint);
            ros::Duration(2).sleep();
            ROS_INFO("Sono in deployment point");
        #if REAL_ARM
            moveFingers(finger_close);
        #endif
            ROS_INFO("Sto andando in check deploy position");
            moveEndEffectorWithConstantOrientation(checkDeployPosition);
            ros::Duration(2).sleep();
            ROS_INFO("Sono in check deploy");
        }
    break;    
    }

    if(deploymentMode == DeploymentMode::MoveIt || deploymentMode == DeploymentMode::VisualServoPlant)
    {
        geometry_msgs::Vector3 offset;
        offset.x = offset.y = 0;
        offset.z = -0.1;
        moveEndEffectorWithConstantOrientation(offset);
    }   

}


//------------------------------------------------------------------------
// Advertised services callbacks
//-----------------------------------------------------------------------

bool grasping_controller::getJointsSafeForWires(vs_grasping::GetJointsSafeForWires::Request &req, vs_grasping::GetJointsSafeForWires::Response &res){
    res.joint0 = jointPositions_untangled.at(0);
    res.joint1 = jointPositions_untangled.at(1);
    res.joint2 = jointPositions_untangled.at(2);
    res.joint3 = jointPositions_untangled.at(3);
    res.joint4 = jointPositions_untangled.at(4);
    res.joint5 = jointPositions_untangled.at(5);

    return true; 
}

bool grasping_controller::armToNavigationPose(grape_msgs::ArmToNavigationPose::Request &req, grape_msgs::ArmToNavigationPose::Response &res){
    ros::AsyncSpinner spinner(1);
    spinner.start();
    res.success = moveArm(navigation_pose);
    untangle_joint0();
    spinner.stop();
    return true;
}


bool grasping_controller::armEmergencyStop(grape_msgs::ArmEmergencyStop::Request &req, grape_msgs::ArmEmergencyStop::Response &res){
    system("rosnode kill /j2n6s300_driver"); 
}


//------------------------------------------------------------------------
// Subscribed topics callbacks
//-----------------------------------------------------------------------

void grasping_controller::toolPose_MessageCallback(const geometry_msgs::PoseStamped::ConstPtr& pose)
{
    toolPose_mutex.lock();
    toolCurrentPose = pose->pose;
    /* Update actual tool position */
    _PeefO(0) = pose->pose.position.x;
    _PeefO(1) = pose->pose.position.y;
    _PeefO(2) = pose->pose.position.z;

    /* Update actual tool orientation executeVisualControl*/
    Eigen::Quaternion<float> quat(pose->pose.orientation.w, 
                                  pose->pose.orientation.x, 
                                  pose->pose.orientation.y, 
                                  pose->pose.orientation.z);
    _ReefO = quat.toRotationMatrix();
    toolPose_mutex.unlock();
}


void grasping_controller::jointPositions_MessageCallback(const kinova_msgs::JointAngles::ConstPtr& jointPositions)
{
    jointPosition_mutex.lock();
    ////////////////////////////////////////
    joint_values = {jointPositions->joint1*M_PI/180.0,
                    jointPositions->joint2*M_PI/180.0,
                    jointPositions->joint3*M_PI/180.0,
                    jointPositions->joint4*M_PI/180.0,
                    jointPositions->joint5*M_PI/180.0,
                    jointPositions->joint6*M_PI/180.0};
    
    jaco2_kinematic_state->setJointGroupPositions(jaco2_joint_model_group, joint_values);
    _jacobian = jaco2_kinematic_state->getJacobian(jaco2_joint_model_group);
    
    // procedures for wires untanglement
    if(rotateEndEffector){
        if(fabs(joint_values.at(5) - targetJointRotation) < distanceFromTargetTolerance) {
            rotateEndEffector = false;
        }
    }
    ////////////////////////////////////////
    jointPosition_mutex.unlock();
}

// rotates the last joint until the rotation angle is in [0;2pi]. 
// use this to disentangle the wires
// It brings the end effector to an angular position equivalent to the current one, but in [-2pi;2pi]
void grasping_controller::rotateEndEffectorBack(){
    // until we don't fix it
    return;

    this->initialJointRotation = joint_values.at(5);
    int entireRotations = initialJointRotation/(2*M_PI);
    // this is for going to [0;2pi]
    // remove this step for going to [-2pi;2pi]
    if(joint_values.at(5) < 0)
        entireRotations = entireRotations - 1;
    targetJointRotation = initialJointRotation - entireRotations*2*M_PI;
    int sign = initialJointRotation > 0 ? -1 : 1;
    end_effector_velocity_cmd.joint1 = 0;
    end_effector_velocity_cmd.joint2 = 0;
    end_effector_velocity_cmd.joint3 = 0;
    end_effector_velocity_cmd.joint4 = 0;
    end_effector_velocity_cmd.joint5 = 0;
    end_effector_velocity_cmd.joint6 = sign*30;
    // activate rotation of the end effector 
    if(entireRotations)
        rotateEndEffector = true;  

    ros::Rate r(NODE_RATE);
    while (rotateEndEffector)
    {
        ros::spinOnce();
        _jointVelocity_publisher.publish(end_effector_velocity_cmd);
        r.sleep();
    }
    ROS_INFO("End effector is in [0;2pi]"); 
}



void grasping_controller::aruco0_markerFeatures_MessageCallback(const vs_msgs::featurePointArray::ConstPtr& marker_features)
{
    actual_arucoFeatures_0_mutex.lock();
    _actual_arucoFeatures_0.clear();
    for (int k=0; k<marker_features->num_featurePoints; k++)
      _actual_arucoFeatures_0.push_back(marker_features->featurePoints.at(k));
    actual_arucoFeatures_0_mutex.unlock();
}


void grasping_controller::aruco1_markerFeatures_MessageCallback(const vs_msgs::featurePointArray::ConstPtr& marker_features)
{
    actual_arucoFeatures_1_mutex.lock();
    _actual_arucoFeatures_1.clear();
    for (int k=0; k<marker_features->num_featurePoints; k++)
      _actual_arucoFeatures_1.push_back(marker_features->featurePoints.at(k));
    actual_arucoFeatures_1_mutex.unlock();
}


void grasping_controller::aruco2_markerFeatures_MessageCallback(const vs_msgs::featurePointArray::ConstPtr& marker_features)
{
    actual_arucoFeatures_2_mutex.lock();
    _actual_arucoFeatures_2.clear();
    for (int k=0; k<marker_features->num_featurePoints; k++)
      _actual_arucoFeatures_2.push_back(marker_features->featurePoints.at(k));
    actual_arucoFeatures_2_mutex.unlock();
}


void grasping_controller::aruco3_markerFeatures_MessageCallback(const vs_msgs::featurePointArray::ConstPtr& marker_features)
{
    actual_arucoFeatures_3_mutex.lock();
    _actual_arucoFeatures_3.clear();
    for (int k=0; k<marker_features->num_featurePoints; k++)
      _actual_arucoFeatures_3.push_back(marker_features->featurePoints.at(k));
    actual_arucoFeatures_3_mutex.unlock();
}


void grasping_controller::aruco4_markerFeatures_MessageCallback(const vs_msgs::featurePointArray::ConstPtr& marker_features)
{
    actual_arucoFeatures_4_mutex.lock();
    _actual_arucoFeatures_4.clear();
    for (int k=0; k<marker_features->num_featurePoints; k++)
      _actual_arucoFeatures_4.push_back(marker_features->featurePoints.at(k));
    actual_arucoFeatures_4_mutex.unlock();
}


void grasping_controller::dispenserCenterCamera_MessageCallback(const geometry_msgs::Point::ConstPtr& dispencerCenter_camera)
{
    actual_dispencerCenter_camera_mutex.lock();
    _actual_dispencerCenter_camera.x = dispencerCenter_camera->x;
    _actual_dispencerCenter_camera.y = dispencerCenter_camera->y;
    _actual_dispencerCenter_camera.z = dispencerCenter_camera->z;
    actual_dispencerCenter_camera_mutex.unlock();
}

void grasping_controller::deploymentPoint_MessageCallback(const vs_msgs::featurePoint& deploymentPoint)
{
    actual_deployFeatures_mutex.lock();
    _actual_deployFeatures = deploymentPoint;
    actual_deployFeatures_mutex.unlock();
}

//------------------------------------------------------------------------
// Arm control
//------------------------------------------------------------------------

void grasping_controller::translatePlan2currentJacoPosition(moveit::planning_interface::MoveGroupInterface::Plan& plan)
{
    // translate all the trajectory points considering the current joint state
    std::vector<double> act_joint_pos = group.getCurrentJointValues();
    for (int i=0; i<plan.trajectory_.joint_trajectory.points.size(); i++) {
        for (int j=0; j<act_joint_pos.size(); j++) {
            plan.trajectory_.joint_trajectory.points.at(i).positions.at(j) += act_joint_pos.at(j)-plan.start_state_.joint_state.position.at(j);
        }
    }

    // translate the initial state
    for (int i=0; i<act_joint_pos.size(); i++) {
        plan.start_state_.joint_state.position.at(i) = act_joint_pos.at(i);
    }   
}

geometry_msgs::Point grasping_controller::getPointAlongDirection(geometry_msgs::Point initialPoint, geometry_msgs::Vector3 direction, float distance){
    geometry_msgs::Point finalPoint;
    // normalize the direction
    float module = sqrt(direction.x*direction.x + direction.y*direction.y + direction.z*direction.z);
    direction.x = (direction.x)/module;
    direction.y = (direction.y)/module;
    direction.z = (direction.z)/module;

    // move along the direction specified
    finalPoint.x = initialPoint.x + distance*direction.x;
    finalPoint.y = initialPoint.y + distance*direction.y;
    finalPoint.z = initialPoint.z + distance*direction.z;

    return finalPoint;
}

void grasping_controller::translateTargetPoint(geometry_msgs::Point& pose)
{   
    Eigen::Matrix3f _RbaseLink_BaseArm;
    _RbaseLink_BaseArm << 0.0,-1.0, 0.0,
                          1.0, 0.0, 0.0,
                          0.0, 0.0, 1.0;
    Eigen::Matrix3f local_ReefO;
    toolPose_mutex.lock();
    local_ReefO = _ReefO;
    toolPose_mutex.unlock();

    Eigen::Vector3f Pdeploy, desired_eef_pose;
    Pdeploy << pose.x, pose.y, pose.z;
    desired_eef_pose = Pdeploy - _RbaseLink_BaseArm*local_ReefO*_PnailsEef;
    pose.x = desired_eef_pose(0);
    pose.y = desired_eef_pose(1);
    pose.z = desired_eef_pose(2);
}


bool grasping_controller::moveEndEffectorWithConstantOrientation(geometry_msgs::Point finalPoint) {
    ros::Duration(1).sleep();
    std::cout << "FRAME: " << group.getPoseReferenceFrame() << std::endl;
    geometry_msgs::Pose currentPose = group.getCurrentPose().pose;
    // geometry_msgs::Pose currentPose = toolCurrentPose;
    
    geometry_msgs::Pose finalPose;
    finalPose.position = finalPoint;
    finalPose.orientation = currentPose.orientation;

    // calculate intermediate steps
    std::vector<geometry_msgs::Pose> waypoints;
    waypoints.push_back(currentPose);
    float deltaX = finalPose.position.x - currentPose.position.x;
    float deltaY = finalPose.position.y - currentPose.position.y;
    float deltaZ = finalPose.position.z - currentPose.position.z;
    int number_intermediate_points = 10;
    float distanceX = deltaX/(number_intermediate_points+1);
    float distanceY = deltaY/(number_intermediate_points+1);
    float distanceZ = deltaZ/(number_intermediate_points+1); 

    geometry_msgs::Pose intermediate_point;
    intermediate_point.orientation = currentPose.orientation;
    for(int i=1; i<number_intermediate_points+1; i++)
    {
        intermediate_point.position.x = currentPose.position.x + distanceX*i;
        intermediate_point.position.y = currentPose.position.y + distanceY*i;
        intermediate_point.position.z = currentPose.position.z + distanceZ*i;
        waypoints.push_back(intermediate_point);
        ROS_INFO("waypoint %d: %f %f %f", i,intermediate_point.position.x, intermediate_point.position.y, intermediate_point.position.z);

    }

    waypoints.push_back(finalPose);
    moveit_msgs::RobotTrajectory trajectory;
    const double jump_threshold = 10;
    const double eef_step = 0.01;
    ros::Duration(0.5).sleep();
    group.setStartStateToCurrentState();
    if(group.computeCartesianPath(waypoints, eef_step, jump_threshold, trajectory) == -1){
        ROS_ERROR("Can't compute trajectory");
    } else {
        moveit_msgs::RobotState initial_state;
        initial_state.joint_state.name = trajectory.joint_trajectory.joint_names;
        initial_state.joint_state.position = trajectory.joint_trajectory.points[0].positions;
        initial_state.joint_state.velocity = {0,0,0,0,0,0};
        initial_state.joint_state.effort = {0,0,0,0,0,0};
        plan.start_state_ = initial_state;
        plan.trajectory_ = trajectory;
        translatePlan2currentJacoPosition(plan);
        group.execute(plan);
    }
}

bool grasping_controller::moveEndEffectorWithConstantOrientation(geometry_msgs::Vector3 offset)
{
    geometry_msgs::Pose currentPose = group.getCurrentPose().pose;
    // geometry_msgs::Pose currentPose = toolCurrentPose;
    ROS_INFO("tool current pose: %f %f %f", toolCurrentPose.position.x, toolCurrentPose.position.y, toolCurrentPose.position.z);
    ROS_INFO("current pose: %f %f %f", currentPose.position.x, currentPose.position.y, currentPose.position.z);
    geometry_msgs::Point finalPoint = currentPose.position;

    finalPoint.x += offset.x;
    finalPoint.y += offset.y;
    finalPoint.z += offset.z;

    return moveEndEffectorWithConstantOrientation(finalPoint);
}

bool grasping_controller::moveAlongZeef(float offset)
{
    Eigen::Matrix3f local_R;
    Eigen::Vector3f delta_eef, delta_O;
    delta_eef << 0,0,offset;
    Eigen::Matrix3f _RbaseLink_BaseArm;
    _RbaseLink_BaseArm << 0.0,-1.0,0.0,
                          1.0, 0.0,0.0,
                          0.0, 0.0,1.0;
    std::cout << "baselink->basearm " <<_RbaseLink_BaseArm << std::endl;


    toolPose_mutex.lock();
    local_R = _ReefO;
    toolPose_mutex.unlock();

    geometry_msgs::Pose currentPose = group.getCurrentPose().pose;
    // geometry_msgs::Pose currentPose = toolCurrentPose;
    geometry_msgs::Point finalPoint = currentPose.position;

    delta_O = _RbaseLink_BaseArm*local_R*delta_eef;
    finalPoint.x += delta_O(0);
    finalPoint.y += delta_O(1);
    finalPoint.z += delta_O(2);

    moveEndEffectorWithConstantOrientation(finalPoint);
}

void grasping_controller::rotation_eef()
{
    kinova_msgs::JointVelocity joint_velocity_cmd;
    joint_velocity_cmd.joint1 = 0;
    joint_velocity_cmd.joint2 = 0;
    joint_velocity_cmd.joint3 = 0;
    joint_velocity_cmd.joint4 = 0;
    joint_velocity_cmd.joint5 = 0;
    joint_velocity_cmd.joint6 = 0;
    
    while(std::fabs(_ReefO(2,0))>0.01)
    {   
        if(_ReefO(2,0)<-0.01)
            joint_velocity_cmd.joint6 = -10;
        else if(_ReefO(2,0)>0.01)
            joint_velocity_cmd.joint6 = 10;

        _jointVelocity_publisher.publish(joint_velocity_cmd);
        ros::Duration(0.01).sleep();
    }
    
}

bool grasping_controller::setEndEffectorPose(geometry_msgs::Point targetPoint){
    geometry_msgs::Pose endEffectorPose;
    Eigen::Matrix3d baseLink_armLink_rotation;
    baseLink_armLink_rotation << 0.0, -1.0, 0.0,
                                 1.0,  0.0, 0.0,
                                 0.0,  0.0, 1.0;
    // from yaml
    Eigen::Quaternion<double> orientation_armBase(Eigen::Vector4d(plantDeploymentOrientation.at(0),
                                                                  plantDeploymentOrientation.at(1),
                                                                  plantDeploymentOrientation.at(2),
                                                                  plantDeploymentOrientation.at(3)));
    Eigen::Quaternion<double> orientation_baseLink(baseLink_armLink_rotation*orientation_armBase.toRotationMatrix());

    std::cout << "target quaternion" 
              << " " << orientation_baseLink.x() 
              << " " << orientation_baseLink.y()
              << " " << orientation_baseLink.z()
              << " " << orientation_baseLink.w() << std::endl;
    
    endEffectorPose.position.x = targetPoint.x;
    endEffectorPose.position.y = targetPoint.y;
    endEffectorPose.position.z = targetPoint.z;
    endEffectorPose.orientation.x = orientation_baseLink.x();
    endEffectorPose.orientation.y =  orientation_baseLink.y();
    endEffectorPose.orientation.z =  orientation_baseLink.z();
    endEffectorPose.orientation.w =  orientation_baseLink.w();
       
    group.setPoseTarget(endEffectorPose);
    ros::Duration(0.5).sleep();
    moveit::planning_interface::MoveItErrorCode ret = group.plan(plan);
    if(ret){
        translatePlan2currentJacoPosition(plan);
        group.execute(plan);
        return true;
    }
    else {
        ROS_ERROR("Cannot plan trajectory. Error code %d", ret.val);
        return false;
    }
}

bool grasping_controller::moveArm(std::vector<double> targetArmPose){
    ros::Duration(0.5).sleep();
    group.setStartStateToCurrentState();
    group.setJointValueTarget(targetArmPose);
    if(group.plan(plan)){
        translatePlan2currentJacoPosition(plan);
        group.execute(plan);
        // rotateEndEffectorBack();
        return true;
    }
    else {
        ROS_INFO("Cannot plan trajectory");
        return false;
    }
}

bool grasping_controller::moveFingers(std::vector<double> targetFingersPose)
{
    actionlib::SimpleActionClient<kinova_msgs::SetFingersPositionAction> ac("/j2n6s300_driver/fingers_action/finger_positions", true);
    ac.waitForServer();

    kinova_msgs::SetFingersPositionGoal goal;
    goal.fingers.finger1 = targetFingersPose.at(0);
    goal.fingers.finger2 = targetFingersPose.at(1);
    goal.fingers.finger3 = targetFingersPose.at(2);
    ac.sendGoal(goal);

    if (ac.waitForResult(ros::Duration(30.0)))
    {
        actionlib::SimpleClientGoalState state = ac.getState();
        return true;
    }

    return false;
}


//------------------------------------------------------------------------
// Visual servo functions
//------------------------------------------------------------------------


// This function computes all velecities for the arm in next time interval
void grasping_controller::visualServoing_PeriodicTask(const vscontroller_type controller_type, const std::vector<vs_msgs::featurePoint>& desired_features, double Z_camera, Eigen::VectorXf& feature_error, int *id_marker, int heightIndex)
{   
    Eigen::Matrix3f local_ReefO;
    Eigen::MatrixXd local_jacobian;

    toolPose_mutex.lock();
    local_ReefO = _ReefO;
    toolPose_mutex.unlock();
    
    jointPosition_mutex.lock();
    local_jacobian = _jacobian;
    jointPosition_mutex.unlock();

    // Set the actual and desired marker features
    std::vector<vs_msgs::featurePoint> local_actual_arucoFeatures, local_desired_features;

    switch (controller_type) {

    case GRASP:
      *id_marker = 0;

      // Try with marker 0
      actual_arucoFeatures_0_mutex.lock();
      local_actual_arucoFeatures = _actual_arucoFeatures_0;
      actual_arucoFeatures_0_mutex.unlock();
      local_desired_features = desired_features;

      // If not all corners are visible, try with marker 1
      if (local_actual_arucoFeatures.size() != desired_features.size()) {
        *id_marker = 1;
        actual_arucoFeatures_1_mutex.lock();
        local_actual_arucoFeatures = _actual_arucoFeatures_1;
        actual_arucoFeatures_1_mutex.unlock();

        // Set desired features accordingly
        for (int i = 0; i < local_desired_features.size(); i++) {
          local_desired_features.at(i).x =
              desired_features.at(i).x +
              _aruco0_to_aruco1_featureTranslation.at(2*heightIndex);
          local_desired_features.at(i).y =
              desired_features.at(i).y +
              _aruco0_to_aruco1_featureTranslation.at(2*heightIndex+1);
        }      
      }

      // If not all corners are visible, try with marker 2
      if (local_actual_arucoFeatures.size() != desired_features.size()) {
        *id_marker = 2;  
        actual_arucoFeatures_2_mutex.lock();
        local_actual_arucoFeatures = _actual_arucoFeatures_2;
        actual_arucoFeatures_2_mutex.unlock();

        // Set desired features accordingly
        for (int i = 0; i < local_desired_features.size(); i++) {
          local_desired_features.at(i).x =
              desired_features.at(i).x +
              _aruco0_to_aruco2_featureTranslation.at(2*heightIndex);
          local_desired_features.at(i).y =
              desired_features.at(i).y +
              _aruco0_to_aruco2_featureTranslation.at(2*heightIndex+1);
        }
      }

      // If not all corners are visible, try with marker 3
      if (local_actual_arucoFeatures.size() != desired_features.size()) {
        *id_marker = 3;  
        actual_arucoFeatures_3_mutex.lock();
        local_actual_arucoFeatures = _actual_arucoFeatures_3;
        actual_arucoFeatures_3_mutex.unlock();

        // Set desired features accordingly
        for (int i = 0; i < local_desired_features.size(); i++) {
          local_desired_features.at(i).x =
              desired_features.at(i).x +
              _aruco0_to_aruco3_featureTranslation.at(2*heightIndex);
          local_desired_features.at(i).y =
              desired_features.at(i).y +
              _aruco0_to_aruco3_featureTranslation.at(2*heightIndex+1);
        }
      }

      break;


    case DEPLOY:
      actual_arucoFeatures_4_mutex.lock();
      local_actual_arucoFeatures = _actual_arucoFeatures_4;
      actual_arucoFeatures_4_mutex.unlock();
      local_desired_features = desired_features;
      *id_marker = 4; 
      break;
    }

    // Compute visual servoing control action
    if (local_actual_arucoFeatures.size()==local_desired_features.size())
    {     
        /* Compute feature errors */
        feature_error = Eigen::VectorXf::Zero(local_desired_features.size()*2);

        for (int k=0; k<local_desired_features.size(); k++)
        {   
            feature_error(2*k)   = local_actual_arucoFeatures.at(k).x - local_desired_features.at(k).x;
            feature_error(2*k+1) = local_actual_arucoFeatures.at(k).y - local_desired_features.at(k).y;
        }

        /* Compute interaction matrix */
        double fx = _colCamera_fx;
        double fy = _colCamera_fy;

        _Ls = Eigen::MatrixXf::Zero(local_desired_features.size()*2, 6);
 
        for (int k=0; k<local_desired_features.size(); k++)
        {   
            double u = local_actual_arucoFeatures.at(k).x-_colCamera_ppx;
            double v = local_actual_arucoFeatures.at(k).y-_colCamera_ppy;

            _Ls.block<2,6>(2*k,0) << -fx/Z_camera,          0.0, u/Z_camera,          u*v/fy, -(fx+powf(u,2)/fx),  v*fx/fy,
                                              0.0, -fy/Z_camera, v/Z_camera, fy+powf(v,2)/fy,            -u*v/fx, -u*fy/fx;
        }

        // Adaptive gain
        double _visual_gain = std::min(k0,(k0 - kinf)*exp(-(kslope/(k0 - kinf))*feature_error.norm()) + kinf);

        /* Visual servoing control */
        _vcam_camera = -_visual_gain*(_Ls.transpose()*_Ls).inverse()*_Ls.transpose()*feature_error;

        /* Convert camera velocity into end-effector velocity */
        Eigen::Vector3f linVel_cam_w = local_ReefO * _RcEef * _vcam_camera.head(3);
        Eigen::Vector3f angVel_cam_w = local_ReefO * _RcEef * _vcam_camera.tail<3>();

        Eigen::VectorXf cartesian_velocity = Eigen::VectorXf::Zero(6);
        cartesian_velocity << linVel_cam_w - angVel_cam_w.cross(local_ReefO*_PcEef), angVel_cam_w;

        _jointVelocity = local_jacobian.fullPivLu().solve(cartesian_velocity.cast<double>())*180/M_PI;
    }
    else
    {
        ROS_INFO("Number of features different than what is expected");

        _jointVelocity = Eigen::VectorXd::Zero(6);
    }
}

/* Partitioned visual servoing (deploy) */
void grasping_controller::visualServoingPartitioned_PeriodicTask(const geometry_msgs::Point P_world, const std::vector<vs_msgs::featurePoint>& desired_features, double Z_camera, Eigen::VectorXf& feature_error, float& z_distance)
{   
    Eigen::Vector3f local_PeefO;
    Eigen::Matrix3f local_ReefO;
    Eigen::MatrixXd local_jacobian;

    Eigen::Vector3f P_w;
    P_w << P_world.x, P_world.y, P_world.z;

    toolPose_mutex.lock();
    local_PeefO = _PeefO;
    local_ReefO = _ReefO;
    toolPose_mutex.unlock();
    
    jointPosition_mutex.lock();
    local_jacobian = _jacobian;
    jointPosition_mutex.unlock();

    vs_msgs::featurePoint local_actual_deployFeatures, local_desired_features;

    actual_deployFeatures_mutex.lock();
    local_actual_deployFeatures = _actual_deployFeatures;
    actual_deployFeatures_mutex.unlock();
    
    if(desired_features.size() == 1 && local_actual_deployFeatures.x != -1 && local_actual_deployFeatures.y != -1) 
    {
        local_desired_features = desired_features.at(0);

        double u = local_actual_deployFeatures.x;
        double v = local_actual_deployFeatures.y;
        double u_des = local_desired_features.x;
        double v_des = local_desired_features.y;

        ROS_INFO("Actual features -- u: %f   v: %f", u,v);
        ROS_INFO("Desired features: %f   %f", u_des,v_des);
        
        /* Compute feature errors */
        feature_error = Eigen::VectorXf::Zero(2);
        feature_error(0) = u - u_des;
        feature_error(1) = v - v_des;

        /* Velocity Z in camera frame */
        Eigen::Vector3f Pdes_eef_eef = local_ReefO.inverse() * (P_w - local_PeefO);
        float z_star = Pdes_eef_eef(2);
        z_distance = z_star;

        float v_cam_cam_z = Ks_z * z_star;
    
        double fx = _colCamera_fx;
        double fy = _colCamera_fy;
        double ppx = _colCamera_ppx;
        double ppy = _colCamera_ppy;
    
        /* Velocities X,Y in camera frame */
        float Z = Z_camera;
        float v_cam_cam_x = (Z/fx)*Ks_xy*(u - u_des) + ((u-ppx)/fx)*v_cam_cam_z;
        float v_cam_cam_y = (Z/fy)*Ks_xy*(v - v_des) + ((v-ppy)/fy)*v_cam_cam_z;

        _vcam_camera << v_cam_cam_x, v_cam_cam_y, v_cam_cam_z, 0,0,0;
        
        /* Convert camera velocity into end-effector velocity */
        Eigen::Vector3f linVel_cam_w = local_ReefO * _RcEef * _vcam_camera.head(3);
        Eigen::Vector3f angVel_cam_w = local_ReefO * _RcEef * _vcam_camera.tail<3>();

        Eigen::VectorXf cartesian_velocity = Eigen::VectorXf::Zero(6);
        cartesian_velocity << linVel_cam_w - angVel_cam_w.cross(local_ReefO*_PcEef), angVel_cam_w;

        _jointVelocity = local_jacobian.fullPivLu().solve(cartesian_velocity.cast<double>())*180/M_PI;
    }
    else
    {
        ROS_INFO("Deployment point is not in sight");

        _jointVelocity = Eigen::VectorXd::Zero(6);
    }
}

bool grasping_controller::computeDesiredFeatures(vs_msgs::featurePointArray& desiredFeatures, float *desiredDepth, int& heightIndex)
{
    geometry_msgs::Point local_actual_dispencerCenter_camera;

    actual_dispencerCenter_camera_mutex.lock();
    local_actual_dispencerCenter_camera = _actual_dispencerCenter_camera;
    actual_dispencerCenter_camera_mutex.unlock();

    // fixed number of features, associated to 4 corners of the marker
    int num_features = 4;
    
    /* Transform dispencerCenter from camera to world */
    geometry_msgs::Point actual_dispencerCenter_world;
    if (!std::isnan(local_actual_dispencerCenter_camera.x)
        || !std::isnan(local_actual_dispencerCenter_camera.y)
        || !std::isnan(local_actual_dispencerCenter_camera.z))
    { 
        camera2world(local_actual_dispencerCenter_camera, actual_dispencerCenter_world);
        ROS_INFO("Dispenser (world): %f, %f, %f\n", actual_dispencerCenter_world.x, actual_dispencerCenter_world.y, actual_dispencerCenter_world.z);
    }
    else
    {
        ROS_ERROR("Dispenser not found");
        return false;
    }

    // Desired features from param server
    for(int i = 0; i<_heightVector.size(); i++)
    {
        heightIndex = i;
        if(actual_dispencerCenter_world.z >= _heightVector.at(i))
            break;
    }
    desiredFeatures.num_featurePoints = 4;
    for(int i=0; i<_defaultFeatures.at(heightIndex).featurePoints.size(); i++)
    {
        desiredFeatures.featurePoints.push_back(_defaultFeatures.at(heightIndex).featurePoints.at(i));
    }
    *desiredDepth = actual_dispencerCenter_world.z + _grasping_frame_offset.at(2);

    return true;
}

// Publish the velocities periodically computed by visualServoing_PeriodicTask function
vs_msgs::featurePointArray grasping_controller::executeVisualControl(const vscontroller_type controller_type, float desiredDepth, vs_msgs::featurePointArray desiredFeatures, int heightIndex) {
    vs_msgs::featurePointArray residualFeatureErrors;

    double Z_camera = desiredDepth;
    ROS_INFO("Z: %f", Z_camera);

    std::vector<vs_msgs::featurePoint> desired_features;
    for (int k = 0; k < desiredFeatures.num_featurePoints; k++)
        desired_features.push_back(desiredFeatures.featurePoints.at(k));

    /* Visual servoing loop */
    ros::Rate LoopRate(1.0 / RunPeriod);

    Eigen::VectorXf feature_errors = 10 * _max_visual_error_norm * Eigen::VectorXf::Ones(desiredFeatures.num_featurePoints * 2);
    int id_marker;
    do {
        /* Compute visual servoing action */
        visualServoing_PeriodicTask(controller_type, desired_features, Z_camera, feature_errors, &id_marker, heightIndex);
        
        /* Send velocity command to the robot */
        kinova_msgs::JointVelocity joint_velocity_cmd;

        joint_velocity_cmd.joint1 = _jointVelocity(0);
        joint_velocity_cmd.joint2 = _jointVelocity(1);
        joint_velocity_cmd.joint3 = _jointVelocity(2);
        joint_velocity_cmd.joint4 = _jointVelocity(3);
        joint_velocity_cmd.joint5 = _jointVelocity(4);
        joint_velocity_cmd.joint6 = _jointVelocity(5);

        _jointVelocity_publisher.publish(joint_velocity_cmd);

        ros::spinOnce();

        LoopRate.sleep();
    } while (feature_errors.norm() >= _max_visual_error_norm);
    
    /* Set response data */
    residualFeatureErrors.num_featurePoints = desiredFeatures.num_featurePoints;
    for (int k = 0; k < desiredFeatures.num_featurePoints; k++) {
        vs_msgs::featurePoint feature_error;
        feature_error.x = feature_errors(2 * k);
        feature_error.y = feature_errors(2 * k + 1);

        residualFeatureErrors.featurePoints.push_back(feature_error);
    }

    return residualFeatureErrors;
}

/* Partitioned visual servoing */
vs_msgs::featurePointArray grasping_controller::executeVisualControlDeplPart(float desiredDepth, vs_msgs::featurePointArray desiredFeatures)
{
    ROS_INFO("desiredFeatures: x:%f y: %f", desiredFeatures.featurePoints[0].x, desiredFeatures.featurePoints[0].y);
    vs_msgs::featurePointArray residualFeatureErrors;

    double Z_camera = desiredDepth + std::fabs(_PcEef(2));
    ROS_INFO("Z: %f", Z_camera);

    std::vector<vs_msgs::featurePoint> desired_features;
    for (int k = 0; k < desiredFeatures.num_featurePoints; k++)
        desired_features.push_back(desiredFeatures.featurePoints.at(k));

    /* Visual servoing loop */
    ros::Rate LoopRate(1.0 / RunPeriod);

    Eigen::VectorXf feature_errors = 10 * _max_visual_error_norm * Eigen::VectorXf::Ones(desiredFeatures.num_featurePoints * 2);
    int id_marker;

    geometry_msgs::Point Pdepl_w, Pdepl_cam;
    Pdepl_w = goal_point;
    Pdepl_cam.x = Pdepl_cam.y = Pdepl_cam.z = 0;
    float distance_z = 1;
    do {
        /* Publish deployment point in camera frame */
        world2camera(Pdepl_w, Pdepl_cam);
        _deploymentPoint_publisher.publish(Pdepl_cam);

        visualServoingPartitioned_PeriodicTask(Pdepl_w, desired_features, Z_camera, feature_errors, distance_z);

        kinova_msgs::JointVelocity joint_velocity_cmd;

        joint_velocity_cmd.joint1 = _jointVelocity(0);
        joint_velocity_cmd.joint2 = _jointVelocity(1);
        joint_velocity_cmd.joint3 = _jointVelocity(2);
        joint_velocity_cmd.joint4 = _jointVelocity(3);
        joint_velocity_cmd.joint5 = _jointVelocity(4);
        joint_velocity_cmd.joint6 = _jointVelocity(5);

        _jointVelocity_publisher.publish(joint_velocity_cmd);

        ROS_INFO("Distance: %f", distance_z);

        ros::spinOnce();

        LoopRate.sleep();
    } while(distance_z > _min_distance_VS);

    /* Set response data */
    residualFeatureErrors.num_featurePoints = desiredFeatures.num_featurePoints;
    for (int k = 0; k < desiredFeatures.num_featurePoints; k++) {
        vs_msgs::featurePoint feature_error;
        feature_error.x = feature_errors(2 * k);
        feature_error.y = feature_errors(2 * k + 1);

        residualFeatureErrors.featurePoints.push_back(feature_error);
    }

    return residualFeatureErrors;
}


//------------------------------------------------------------------------
// Helper functions
//------------------------------------------------------------------------


void grasping_controller::initManipulator(){
    group.setPlannerId("PRMstarkConfigDefault");
    group.setPlanningTime(8.0);
    group.setGoalPositionTolerance(0.0001);
    group.setGoalOrientationTolerance(0.001);
    group.setGoalJointTolerance(0.0001);
    //group.setPoseReferenceFrame("j2n6s300_link_base");
}

void grasping_controller::camera2world(geometry_msgs::Point P_camera, geometry_msgs::Point& P_world)
{
    Eigen::Vector3f dispenser_center_world_vect = _PeefO + _ReefO*_PcEef + _ReefO*_RcEef*Eigen::Vector3f(P_camera.x, P_camera.y, P_camera.z);

    P_world.x = dispenser_center_world_vect(0);
    P_world.y = dispenser_center_world_vect(1);
    P_world.z = dispenser_center_world_vect(2);
}

void grasping_controller::world2camera(geometry_msgs::Point P_world, geometry_msgs::Point& P_camera)
{   
    Eigen::Vector3f local_PeefO;
    Eigen::Matrix3f local_ReefO;

    toolPose_mutex.lock();
    local_PeefO = _PeefO;
    local_ReefO = _ReefO;
    toolPose_mutex.unlock();

    Eigen::Vector3f p_cam = (local_ReefO*_RcEef).transpose()*(Eigen::Vector3f(P_world.x, P_world.y, P_world.z) - local_PeefO - local_ReefO*_PcEef);
    
    P_camera.x = p_cam(0);
    P_camera.y = p_cam(1);
    P_camera.z = p_cam(2);
}

void grasping_controller::initParameters(){

    RunPeriod = RUN_PERIOD_DEFAULT;
    /* Retrieve parameters from ROS parameter server */
    std::string FullParamName;

    FullParamName = ros::this_node::getName()+"/run_period";
    if (false == Handle.getParam(FullParamName, RunPeriod))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    FullParamName = ros::this_node::getName()+"/k0";
    if (false == Handle.getParam(FullParamName, k0))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    FullParamName = ros::this_node::getName()+"/kinf";
    if (false == Handle.getParam(FullParamName, kinf))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    FullParamName = ros::this_node::getName()+"/kslope";
    if (false == Handle.getParam(FullParamName, kslope))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    FullParamName = ros::this_node::getName()+"/ks_z";
    if (false == Handle.getParam(FullParamName, Ks_z))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    FullParamName = ros::this_node::getName()+"/ks_xy";
    if (false == Handle.getParam(FullParamName, Ks_xy))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    FullParamName = ros::this_node::getName()+"/max_visual_error_norm";
    if (false == Handle.getParam(FullParamName, _max_visual_error_norm))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    FullParamName = ros::this_node::getName()+"/min_distance_VS";
    if (false == Handle.getParam(FullParamName, _min_distance_VS))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    FullParamName = ros::this_node::getName()+"/grasping_frame_offset";
    if (false == Handle.getParam(FullParamName, _grasping_frame_offset))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    //Desired features
    std::vector<double> heights;
    FullParamName = ros::this_node::getName()+"/heightVector";
    if (false == Handle.getParam(FullParamName, heights))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    for(int j=0; j<heights.size(); j++)
    {
        _heightVector.push_back(heights.at(j));
    }

    std::vector<int> features;
    FullParamName = ros::this_node::getName()+"/recordedFeatures";
    if (false == Handle.getParam(FullParamName, features))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    for(int i=0; i<features.size(); i=i+8)
    {   
        vs_msgs::featurePointArray featureSet;
        for(int j=0; j<8; j=j+2)
        {
            vs_msgs::featurePoint feat;
            feat.x = features.at(i+j);
            feat.y = features.at(i+j+1);
            featureSet.featurePoints.push_back(feat);
        }
        _defaultFeatures.push_back(featureSet);
    }

    features.clear();
    FullParamName = ros::this_node::getName() + "/deployingDesiredFeatures";
    if (false == Handle.getParam(FullParamName, features))
      ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    for (int i=0; i<features.size(); i=i+2) {
        vs_msgs::featurePoint featurePoint;
        featurePoint.x = features.at(i);
        featurePoint.y = features.at(i+1);
        _deployingDesiredFeatures.push_back(featurePoint);
    }

    features.clear();
    FullParamName = ros::this_node::getName() + "/deployingDispenserCenter";
    if (false == Handle.getParam(FullParamName, features))
      ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    _deployingDispenserCenter.x = features.at(0);
    _deployingDispenserCenter.y = features.at(1);


    FullParamName = ros::this_node::getName()+"/aruco0_to_aruco1";
    if (false == Handle.getParam(FullParamName, _aruco0_to_aruco1_featureTranslation))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    FullParamName = ros::this_node::getName()+"/aruco0_to_aruco2";
    if (false == Handle.getParam(FullParamName, _aruco0_to_aruco2_featureTranslation))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    FullParamName = ros::this_node::getName()+"/aruco0_to_aruco3";
    if (false == Handle.getParam(FullParamName, _aruco0_to_aruco3_featureTranslation))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());


    // RcEef: rotation camera->end effector 
    std::vector<double> RcEef;
    FullParamName = ros::this_node::getName()+"/RcEef";
    if (false == Handle.getParam(FullParamName, RcEef))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    
    for (int j=0; j<3; j++)
        for (int k=0; k<3; k++)
            _RcEef(j,k) = RcEef.at(3*j+k);

    // PcEef: position camera->end effector 
    std::vector<double> PcEef;
    FullParamName = ros::this_node::getName()+"/PcEef";
    if (false == Handle.getParam(FullParamName, PcEef))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    
    for (int k=0; k<3; k++)
        _PcEef(k) = PcEef.at(k);

    // PnailsEef: position nails->end effector
    std::vector<double> PnailsEef;
    FullParamName = ros::this_node::getName()+"/PnailsEef";
    if (false == Handle.getParam(FullParamName, PnailsEef))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    
    for (int k=0; k<3; k++)
        _PnailsEef(k) = PnailsEef.at(k);

    // Init the available positions of the arm
    FullParamName = ros::this_node::getName()+"/navigation_pose";
    if (false == Handle.getParam(FullParamName, navigation_pose))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    FullParamName = ros::this_node::getName()+"/test_pose";
    if (false == Handle.getParam(FullParamName, test_pose))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    
    FullParamName = ros::this_node::getName()+"/prepicking_pose_0";
    if (false == Handle.getParam(FullParamName, prepicking_poses.at(0)))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
   FullParamName = ros::this_node::getName()+"/prepicking_pose_1";
    if (false == Handle.getParam(FullParamName, prepicking_poses.at(1)))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    FullParamName = ros::this_node::getName()+"/vis_servoing_pose_0";
    if (false == Handle.getParam(FullParamName, vis_servoing_poses.at(0)))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    FullParamName = ros::this_node::getName()+"/vis_servoing_pose_1";
    if (false == Handle.getParam(FullParamName, vis_servoing_poses.at(1)))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

   FullParamName = ros::this_node::getName()+"/nail_pose";
    if (false == Handle.getParam(FullParamName, nail_pose))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    
    std::vector<double> tmp;
    FullParamName = ros::this_node::getName()+"/postpicking_offset";
    if (false == Handle.getParam(FullParamName, tmp))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    postpicking_offset.x = tmp.at(0);
    postpicking_offset.y = tmp.at(1);
    postpicking_offset.z = tmp.at(2);
    tmp.clear();
    FullParamName = ros::this_node::getName()+"/pre_deploy_VS_offset";
    if (false == Handle.getParam(FullParamName, tmp))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    pre_deploy_VS_offset.x = tmp.at(0);
    pre_deploy_VS_offset.y = tmp.at(1);
    pre_deploy_VS_offset.z = tmp.at(2);

    FullParamName = ros::this_node::getName()+"/post_deploy_nail_offset";
    if (false == Handle.getParam(FullParamName, post_deploy_nail_offset))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    FullParamName = ros::this_node::getName()+"/deploy_VS_last_step";
    if (false == Handle.getParam(FullParamName, deploy_VS_last_step))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    // Init the available positions of the fingers
    FullParamName = ros::this_node::getName()+"/finger_close";
    if (false == Handle.getParam(FullParamName, finger_close))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    FullParamName = ros::this_node::getName()+"/finger_open";
    if (false == Handle.getParam(FullParamName, finger_open))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    FullParamName = ros::this_node::getName()+"/finger_prepick_position";
    if (false == Handle.getParam(FullParamName, finger_prepick_position))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    FullParamName = ros::this_node::getName()+"/finger_pick_position";
    if (false == Handle.getParam(FullParamName, finger_pick_position))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    // arm parameters
    FullParamName = ros::this_node::getName()+"/plant_safety_distance";
    if (false == Handle.getParam(FullParamName, plantSafetyDistance))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    
    FullParamName = ros::this_node::getName()+"/check_deployment_distance";
    if (false == Handle.getParam(FullParamName, checkDeploymentDistance))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());

    FullParamName = ros::this_node::getName()+"/check_deployment_distance";
    if (false == Handle.getParam(FullParamName, checkDeploymentDistance))
      ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    
    FullParamName = ros::this_node::getName()+"/plant_deployment_orientation";
    if (false == Handle.getParam(FullParamName, plantDeploymentOrientation))
       ROS_ERROR("Node %s: unable to retrieve parameter %s.", ros::this_node::getName().c_str(), FullParamName.c_str());
    
    Handle.param<double>("/scan_plant_server/distance_from_target_tolerance", distanceFromTargetTolerance, 0.05);
    /* Initialize node state */
    _ReefO = Eigen::Matrix3f::Zero();
    _PeefO = Eigen::Vector3f::Zero();

    _Ls = Eigen::MatrixXf::Zero(8,6);
    _vcam_camera = Eigen::VectorXf::Zero(6);
    _actual_arucoFeatures_0.clear();
    _actual_arucoFeatures_1.clear();
    _actual_arucoFeatures_2.clear();
    _actual_arucoFeatures_3.clear();
    _actual_arucoFeatures_4.clear();
    // _actual_arucoCamera_0.clear();

    _actual_dispencerCenter_camera.x = _actual_dispencerCenter_camera.y = _actual_dispencerCenter_camera.z = std::nan("");
}
